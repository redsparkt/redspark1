namespace theratrove.data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TherapistTypesOfTherapy")]
    public partial class TherapistTypesOfTherapy
    {
        public int Id { get; set; }

        public int? TherapistID { get; set; }

        public int? TherapyTypesID { get; set; }

        public virtual Therapist Therapist { get; set; }

        public virtual TherapyType TherapyType { get; set; }
    }
}
