namespace theratrove.data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Therapist")]
    public partial class Therapist
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Therapist()
        {
            TherapistExpertises = new HashSet<TherapistExpertise>();
            TherapistExpertiseFaiths = new HashSet<TherapistExpertiseFaith>();
            TherapistExpertiseFors = new HashSet<TherapistExpertiseFor>();
            TherapistExpertiseGenderPreferences = new HashSet<TherapistExpertiseGenderPreference>();
            TherapistExpertiseInsurances = new HashSet<TherapistExpertiseInsurance>();
            TherapistExpertiseIssues = new HashSet<TherapistExpertiseIssue>();
            TherapistExpertiseLanguages = new HashSet<TherapistExpertiseLanguage>();
            TherapistExpertiseLocations = new HashSet<TherapistExpertiseLocation>();
            TherapistRatings = new HashSet<TherapistRating>();
            TherapistTypesOfTherapies = new HashSet<TherapistTypesOfTherapy>();
            TherapyGalleries = new HashSet<TherapyGallery>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string LastName { get; set; }

        [StringLength(100)]
        public string Title { get; set; }

        [StringLength(20)]
        public string YearsInService { get; set; }

        [StringLength(100)]
        public string Education { get; set; }

        public bool AcceptInsurance { get; set; }

        [StringLength(50)]
        public string License { get; set; }

        public string AboutMe { get; set; }

        [StringLength(200)]
        public string VideoUrl { get; set; }

        public bool? Active { get; set; }

        public DateTime? CreateDate { get; set; }

        [StringLength(50)]
        public string PhoneNumber { get; set; }

        [StringLength(50)]
        public string Location { get; set; }

        public int? AvgCost { get; set; }

        [StringLength(200)]
        public string Address1 { get; set; }

        [StringLength(200)]
        public string Address2 { get; set; }

        [StringLength(100)]
        public string City { get; set; }

        [StringLength(50)]
        public string State { get; set; }

        [StringLength(8)]
        public string ZipCode { get; set; }

        public byte? Gender { get; set; }

        public int LikeCount { get; set; }

        public virtual User User { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TherapistExpertise> TherapistExpertises { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TherapistExpertiseFaith> TherapistExpertiseFaiths { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TherapistExpertiseFor> TherapistExpertiseFors { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TherapistExpertiseGenderPreference> TherapistExpertiseGenderPreferences { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TherapistExpertiseInsurance> TherapistExpertiseInsurances { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TherapistExpertiseIssue> TherapistExpertiseIssues { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TherapistExpertiseLanguage> TherapistExpertiseLanguages { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TherapistExpertiseLocation> TherapistExpertiseLocations { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TherapistRating> TherapistRatings { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TherapistTypesOfTherapy> TherapistTypesOfTherapies { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TherapyGallery> TherapyGalleries { get; set; }
    }
}
