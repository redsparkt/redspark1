namespace theratrove.data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Testimonial")]
    public partial class Testimonial
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(500)]
        public string Details { get; set; }

        [Required]
        [StringLength(50)]
        public string Designation { get; set; }

        public bool? Active { get; set; }

        public DateTime? CreateDate { get; set; }
    }
}
