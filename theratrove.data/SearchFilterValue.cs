namespace theratrove.data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SearchFilterValue
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SearchFilterValue()
        {
            TherapistExpertises = new HashSet<TherapistExpertise>();
            TherapistExpertiseFaiths = new HashSet<TherapistExpertiseFaith>();
            TherapistExpertiseFors = new HashSet<TherapistExpertiseFor>();
            TherapistExpertiseGenderPreferences = new HashSet<TherapistExpertiseGenderPreference>();
            TherapistExpertiseInsurances = new HashSet<TherapistExpertiseInsurance>();
            TherapistExpertiseIssues = new HashSet<TherapistExpertiseIssue>();
            TherapistExpertiseLanguages = new HashSet<TherapistExpertiseLanguage>();
            TherapistExpertiseLocations = new HashSet<TherapistExpertiseLocation>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(500)]
        public string Name { get; set; }

        [StringLength(500)]
        public string Value { get; set; }

        public int SearchFilterID { get; set; }

        public bool? Active { get; set; }

        public DateTime? CreateDate { get; set; }

        public int DisplayOrder { get; set; }

        public virtual SearchFilter SearchFilter { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TherapistExpertise> TherapistExpertises { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TherapistExpertiseFaith> TherapistExpertiseFaiths { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TherapistExpertiseFor> TherapistExpertiseFors { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TherapistExpertiseGenderPreference> TherapistExpertiseGenderPreferences { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TherapistExpertiseInsurance> TherapistExpertiseInsurances { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TherapistExpertiseIssue> TherapistExpertiseIssues { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TherapistExpertiseLanguage> TherapistExpertiseLanguages { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TherapistExpertiseLocation> TherapistExpertiseLocations { get; set; }
    }
}
