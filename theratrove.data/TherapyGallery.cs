namespace theratrove.data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TherapyGallery")]
    public partial class TherapyGallery
    {
        public int Id { get; set; }

        public int? UserId { get; set; }

        public DateTime? CreateDateTime { get; set; }

        [StringLength(1000)]
        public string GalleryImageURL { get; set; }


        [Newtonsoft.Json.JsonIgnore]
        public virtual Therapist Therapist { get; set; }

        [Newtonsoft.Json.JsonIgnore]
        public virtual User User { get; set; }
    }
}
