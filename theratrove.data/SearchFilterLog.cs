namespace theratrove.data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SearchFilterLog
    {
        public int Id { get; set; }

        public DateTime? DateCreated { get; set; }

        [StringLength(100)]
        public string Keyword { get; set; }

        [StringLength(50)]
        public string City { get; set; }

        [StringLength(50)]
        public string State { get; set; }

        [StringLength(50)]
        public string Country { get; set; }

        [StringLength(50)]
        public string Zip { get; set; }

        public string Issues { get; set; }

        public string Insurance { get; set; }

        public string Gender { get; set; }

        public string Language { get; set; }

        public string Faith { get; set; }

        public string LookingFor { get; set; }

        [StringLength(50)]
        public string IPAddress { get; set; }

        public string Location { get; set; }

        [StringLength(1000)]
        public string Condition { get; set; }
    }
}
