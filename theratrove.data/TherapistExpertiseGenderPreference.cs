namespace theratrove.data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TherapistExpertiseGenderPreference")]
    public partial class TherapistExpertiseGenderPreference
    {
        public int Id { get; set; }

        public int TherapistId { get; set; }

        public int SearchFilterValueId { get; set; }

        public int SearchFilterId { get; set; }

        public virtual SearchFilter SearchFilter { get; set; }

        public virtual SearchFilterValue SearchFilterValue { get; set; }

        public virtual Therapist Therapist { get; set; }
    }
}
