namespace theratrove.data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PageContent")]
    public partial class PageContent
    {
        public int Id { get; set; }

        [StringLength(50)]
        public string PageName { get; set; }

        [Column("PageContent")]
        public string PageContent1 { get; set; }

        public DateTime? CreateDate { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}
