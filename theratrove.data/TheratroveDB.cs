namespace theratrove.data
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class TheratroveDB : DbContext
    {
        public TheratroveDB()
            : base("name=TheratroveDB")
        {
        }

        public virtual DbSet<ContactInfo> ContactInfoes { get; set; }
        public virtual DbSet<PageContent> PageContents { get; set; }
        public virtual DbSet<SearchFilter> SearchFilters { get; set; }
        public virtual DbSet<SearchFilterLog> SearchFilterLogs { get; set; }
        public virtual DbSet<SearchFilterValue> SearchFilterValues { get; set; }
        public virtual DbSet<SearchLog> SearchLogs { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<Testimonial> Testimonials { get; set; }
        public virtual DbSet<Therapist> Therapists { get; set; }
        public virtual DbSet<TherapistExpertise> TherapistExpertises { get; set; }
        public virtual DbSet<TherapistExpertiseFaith> TherapistExpertiseFaiths { get; set; }
        public virtual DbSet<TherapistExpertiseFor> TherapistExpertiseFors { get; set; }
        public virtual DbSet<TherapistExpertiseGenderPreference> TherapistExpertiseGenderPreferences { get; set; }
        public virtual DbSet<TherapistExpertiseInsurance> TherapistExpertiseInsurances { get; set; }
        public virtual DbSet<TherapistExpertiseIssue> TherapistExpertiseIssues { get; set; }
        public virtual DbSet<TherapistExpertiseLanguage> TherapistExpertiseLanguages { get; set; }
        public virtual DbSet<TherapistExpertiseLocation> TherapistExpertiseLocations { get; set; }
        public virtual DbSet<TherapistLike> TherapistLikes { get; set; }
        public virtual DbSet<TherapistRating> TherapistRatings { get; set; }
        public virtual DbSet<TherapistTypesOfTherapy> TherapistTypesOfTherapies { get; set; }
        public virtual DbSet<TherapyGallery> TherapyGalleries { get; set; }
        public virtual DbSet<TherapyType> TherapyTypes { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ContactInfo>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<ContactInfo>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<ContactInfo>()
                .Property(e => e.Subject)
                .IsUnicode(false);

            modelBuilder.Entity<ContactInfo>()
                .Property(e => e.Message)
                .IsUnicode(false);

            modelBuilder.Entity<PageContent>()
                .Property(e => e.PageName)
                .IsUnicode(false);

            modelBuilder.Entity<SearchFilter>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<SearchFilter>()
                .HasMany(e => e.SearchFilterValues)
                .WithRequired(e => e.SearchFilter)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SearchFilter>()
                .HasMany(e => e.TherapistExpertises)
                .WithRequired(e => e.SearchFilter)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SearchFilter>()
                .HasMany(e => e.TherapistExpertiseFaiths)
                .WithRequired(e => e.SearchFilter)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SearchFilter>()
                .HasMany(e => e.TherapistExpertiseFors)
                .WithRequired(e => e.SearchFilter)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SearchFilter>()
                .HasMany(e => e.TherapistExpertiseGenderPreferences)
                .WithRequired(e => e.SearchFilter)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SearchFilter>()
                .HasMany(e => e.TherapistExpertiseInsurances)
                .WithRequired(e => e.SearchFilter)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SearchFilter>()
                .HasMany(e => e.TherapistExpertiseIssues)
                .WithRequired(e => e.SearchFilter)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SearchFilter>()
                .HasMany(e => e.TherapistExpertiseLanguages)
                .WithRequired(e => e.SearchFilter)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SearchFilter>()
                .HasMany(e => e.TherapistExpertiseLocations)
                .WithRequired(e => e.SearchFilter)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SearchFilterLog>()
                .Property(e => e.Keyword)
                .IsUnicode(false);

            modelBuilder.Entity<SearchFilterLog>()
                .Property(e => e.City)
                .IsUnicode(false);

            modelBuilder.Entity<SearchFilterLog>()
                .Property(e => e.State)
                .IsUnicode(false);

            modelBuilder.Entity<SearchFilterLog>()
                .Property(e => e.Country)
                .IsUnicode(false);

            modelBuilder.Entity<SearchFilterLog>()
                .Property(e => e.Zip)
                .IsUnicode(false);

            modelBuilder.Entity<SearchFilterLog>()
                .Property(e => e.Issues)
                .IsUnicode(false);

            modelBuilder.Entity<SearchFilterLog>()
                .Property(e => e.Insurance)
                .IsUnicode(false);

            modelBuilder.Entity<SearchFilterLog>()
                .Property(e => e.Gender)
                .IsUnicode(false);

            modelBuilder.Entity<SearchFilterLog>()
                .Property(e => e.Language)
                .IsUnicode(false);

            modelBuilder.Entity<SearchFilterLog>()
                .Property(e => e.Faith)
                .IsUnicode(false);

            modelBuilder.Entity<SearchFilterLog>()
                .Property(e => e.LookingFor)
                .IsUnicode(false);

            modelBuilder.Entity<SearchFilterLog>()
                .Property(e => e.IPAddress)
                .IsUnicode(false);

            modelBuilder.Entity<SearchFilterLog>()
                .Property(e => e.Location)
                .IsUnicode(false);

            modelBuilder.Entity<SearchFilterLog>()
                .Property(e => e.Condition)
                .IsUnicode(false);

            modelBuilder.Entity<SearchFilterValue>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<SearchFilterValue>()
                .Property(e => e.Value)
                .IsUnicode(false);

            modelBuilder.Entity<SearchFilterValue>()
                .HasMany(e => e.TherapistExpertises)
                .WithRequired(e => e.SearchFilterValue)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SearchFilterValue>()
                .HasMany(e => e.TherapistExpertiseFaiths)
                .WithRequired(e => e.SearchFilterValue)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SearchFilterValue>()
                .HasMany(e => e.TherapistExpertiseFors)
                .WithRequired(e => e.SearchFilterValue)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SearchFilterValue>()
                .HasMany(e => e.TherapistExpertiseGenderPreferences)
                .WithRequired(e => e.SearchFilterValue)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SearchFilterValue>()
                .HasMany(e => e.TherapistExpertiseInsurances)
                .WithRequired(e => e.SearchFilterValue)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SearchFilterValue>()
                .HasMany(e => e.TherapistExpertiseIssues)
                .WithRequired(e => e.SearchFilterValue)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SearchFilterValue>()
                .HasMany(e => e.TherapistExpertiseLanguages)
                .WithRequired(e => e.SearchFilterValue)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SearchFilterValue>()
                .HasMany(e => e.TherapistExpertiseLocations)
                .WithRequired(e => e.SearchFilterValue)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SearchLog>()
                .Property(e => e.Value)
                .IsUnicode(false);

            modelBuilder.Entity<SearchLog>()
                .Property(e => e.IPAddress)
                .IsUnicode(false);

            modelBuilder.Entity<SearchLog>()
                .Property(e => e.SearchFilterName)
                .IsUnicode(false);

            modelBuilder.Entity<SearchLog>()
                .Property(e => e.City)
                .IsUnicode(false);

            modelBuilder.Entity<SearchLog>()
                .Property(e => e.State)
                .IsUnicode(false);

            modelBuilder.Entity<SearchLog>()
                .Property(e => e.Country)
                .IsUnicode(false);

            modelBuilder.Entity<Testimonial>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Testimonial>()
                .Property(e => e.Details)
                .IsUnicode(false);

            modelBuilder.Entity<Testimonial>()
                .Property(e => e.Designation)
                .IsUnicode(false);

            modelBuilder.Entity<Therapist>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<Therapist>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<Therapist>()
                .Property(e => e.Title)
                .IsUnicode(false);

            modelBuilder.Entity<Therapist>()
                .Property(e => e.YearsInService)
                .IsUnicode(false);

            modelBuilder.Entity<Therapist>()
                .Property(e => e.Education)
                .IsUnicode(false);

            modelBuilder.Entity<Therapist>()
                .Property(e => e.License)
                .IsUnicode(false);

            modelBuilder.Entity<Therapist>()
                .Property(e => e.AboutMe)
                .IsUnicode(false);

            modelBuilder.Entity<Therapist>()
                .Property(e => e.VideoUrl)
                .IsUnicode(false);

            modelBuilder.Entity<Therapist>()
                .Property(e => e.PhoneNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Therapist>()
                .Property(e => e.Location)
                .IsUnicode(false);

            modelBuilder.Entity<Therapist>()
                .Property(e => e.Address1)
                .IsUnicode(false);

            modelBuilder.Entity<Therapist>()
                .Property(e => e.Address2)
                .IsUnicode(false);

            modelBuilder.Entity<Therapist>()
                .Property(e => e.City)
                .IsUnicode(false);

            modelBuilder.Entity<Therapist>()
                .Property(e => e.State)
                .IsUnicode(false);

            modelBuilder.Entity<Therapist>()
                .Property(e => e.ZipCode)
                .IsUnicode(false);

            modelBuilder.Entity<Therapist>()
                .HasMany(e => e.TherapistRatings)
                .WithRequired(e => e.Therapist)
                .HasForeignKey(e => e.TherapisttId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Therapist>()
                .HasMany(e => e.TherapistTypesOfTherapies)
                .WithOptional(e => e.Therapist)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Therapist>()
                .HasMany(e => e.TherapyGalleries)
                .WithOptional(e => e.Therapist)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<TherapistRating>()
                .Property(e => e.IPAddress)
                .IsUnicode(false);

            modelBuilder.Entity<TherapistRating>()
                .Property(e => e.Rating)
                .HasPrecision(16, 2);

            modelBuilder.Entity<TherapyGallery>()
                .Property(e => e.GalleryImageURL)
                .IsUnicode(false);

            modelBuilder.Entity<TherapyType>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<TherapyType>()
                .Property(e => e.Value)
                .IsUnicode(false);

            modelBuilder.Entity<TherapyType>()
                .HasMany(e => e.TherapistTypesOfTherapies)
                .WithOptional(e => e.TherapyType)
                .HasForeignKey(e => e.TherapyTypesID);

            modelBuilder.Entity<User>()
                .Property(e => e.UserName)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Password)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.ActivationCode)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.ProfilePicURL)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .HasOptional(e => e.Therapist)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete();

            modelBuilder.Entity<User>()
                .HasMany(e => e.TherapistLikes)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.TherapistId);

            modelBuilder.Entity<User>()
                .HasMany(e => e.TherapistLikes1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<User>()
                .HasMany(e => e.TherapyGalleries)
                .WithOptional(e => e.User)
                .WillCascadeOnDelete();
        }
    }
}
