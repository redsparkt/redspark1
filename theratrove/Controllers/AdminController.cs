﻿using MvcBreadCrumbs;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using theratrove.data;
using theratrove.Helper;
using theratrove.Models;

using LinqToExcel;
using System.IO;
using System.Data.Entity.Validation;
using System.Web.Routing;
using Newtonsoft.Json;
using System.IO.Compression;
using System.Net.Mail;
using PagedList;

namespace theratrove.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        [CustomAuthorization(LoginPage = Constants.AdminLogingURL)]
        public ActionResult Index()
        {
             
            return View();
        }

        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult ForgetPassword()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ForgetPassword(string userName)
        {
            var theratroveDB = new TheratroveDB();
            var user = theratroveDB.Users.Where(e => (e.UserName == userName || e.Email == userName) && e.UserType == (byte)UserTypeEnum.Admin).FirstOrDefault();
            if (user == null)
            {
                ViewBag.ErrorMessage = "username or email does not exists";
                return View("ForgetPassword");
            }
            else
            {
                string mailBodyhtml = "Your Theratrove password  is: " + user.Password;
                MailAddress mailAddressTo = new MailAddress(user.Email, "Theratrove");
                theratrove.Infrastructure.Helper.SendEmail(mailAddressTo, mailBodyhtml, "Theratrove");
            }
            ViewBag.ErrorMessage = "password sent on your email address";
            return View("ForgetPassword");
        }

        [HttpPost]
        public ActionResult Login(string userName, string password)
        {
            var theratroveDB = new TheratroveDB();
            ViewBag.ErrorMessage = "";
            var user = theratroveDB.Users.Where(e => e.UserName == userName && e.UserType == (byte)UserTypeEnum.Admin && e.Active == true).FirstOrDefault();
            if (user == null)
            {
                ViewBag.ErrorMessage = "username/password did not match";
                return View("Login");
            }
            else
            {
                if (user.Password == password)
                {
                    user.LastLogin = DateTime.Now;
                    theratroveDB.SaveChanges();
                    ViewBag.SucessMessage = "Successfully logged in";
                    Session["UserId"] = user.UserId;
                    Session["UserName"] = user.UserName;
                    Session["UserType"] = user.UserType;
                    Session["Email"] = user.Email;

                    FormsAuthentication.SetAuthCookie(userName, false);

                    return RedirectToAction("Index", "Admin");
                }
                else
                {
                    user.FailedLoginAttempts = user.FailedLoginAttempts.GetValueOrDefault(0) + 1;
                    theratroveDB.SaveChanges();
                    ViewBag.ErrorMessage = "username/password did not match";
                    return View("Login");
                }
            }
        }


        //[CustomAuthorization(LoginPage = Constants.AdminLogingURL)]
        //public ActionResult UserList()
        //{
        //    CheckLogin();
        //    UserModel userModel = new UserModel();
        //    var theratroveDB = new TheratroveDB();
        //    userModel.UserList = theratroveDB.Users.Where(f => f.UserType == (byte)UserTypeEnum.Admin).ToList();
        //    return View(userModel);
        //}
        // GET: /Employee/  
        [CustomAuthorization(LoginPage = Constants.AdminLogingURL)]
        public ActionResult UserList(string sortOrder, string CurrentSort, int? page, string OrderBy)
        {
            var db = new TheratroveDB();
            UserModel userModel = new UserModel();
            int pageSize = 20;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            if (string.IsNullOrEmpty(OrderBy))
            {
                OrderBy = "asc";
            }
            if (pageIndex == 1)
            {
                if (OrderBy == "desc")
                    OrderBy = "asc";
                else
                    OrderBy = "desc";
                ViewBag.OrderBy = OrderBy;
            }
            else
            {
                ViewBag.CurrentSort = sortOrder;
            }
            sortOrder = String.IsNullOrEmpty(sortOrder) ? "ID" : sortOrder;
            IPagedList<User> user = null;
            switch (sortOrder)
            {
                case "ID":
                    if (OrderBy.Equals("desc"))
                        user = db.Users.OrderByDescending
                                (m => m.UserId).ToPagedList(pageIndex, pageSize);
                    else
                        user = db.Users.OrderBy
                                (m => m.UserId).ToPagedList(pageIndex, pageSize);
                    break;
                case "Username":
                    if (OrderBy.Equals("desc"))
                        user = db.Users.OrderByDescending
                                (m => m.UserName).ToPagedList(pageIndex, pageSize);
                    else
                        user = db.Users.OrderBy
                                (m => m.UserName).ToPagedList(pageIndex, pageSize);
                    break;
                case "Email":
                    if (OrderBy.Equals("desc"))
                        user = db.Users.OrderByDescending
                                (m => m.Email).ToPagedList(pageIndex, pageSize);
                    else
                        user = db.Users.OrderBy
                                (m => m.Email).ToPagedList(pageIndex, pageSize);
                    break;
                case "Active":
                    if (OrderBy.Equals("desc"))
                        user = db.Users.OrderByDescending
                                (m => m.Active).ToPagedList(pageIndex, pageSize);
                    else
                        user = db.Users.OrderBy
                                (m => m.Active).ToPagedList(pageIndex, pageSize);
                    break;
                case "Default":
                    user = db.Users.OrderBy
                        (m => m.UserId).ToPagedList(pageIndex, pageSize);
                    break;
            }
            
            return View(user);
        }
        [CustomAuthorization(LoginPage = Constants.AdminLogingURL)]
        public ActionResult User(int? userId)
        {
            CheckLogin();
            SignUpVM userModel = new SignUpVM();
            if (userId > 0)
            {
                ViewBag.Message = "Edit Admin User";
                var theratroveDB = new TheratroveDB();
                var user = theratroveDB.Users.Where(f => f.UserType == (byte)UserTypeEnum.Admin && f.UserId == userId).FirstOrDefault();
                userModel.UserId = user.UserId;
                userModel.Email = user.Email;
                userModel.UserName = user.UserName;
                userModel.Password = user.Password;
                userModel.ConfirmPassword = user.Password;
                userModel.Active = user.Active;
            }
            else
            {
                ViewBag.Message = "Create Admin User";
            }
            return View(userModel);
        }
        [HttpPost]
        public ActionResult User(SignUpVM userModel)
        {
            CheckLogin();
            if (ModelState.IsValid)
            {
                var theratroveDB = new TheratroveDB();
                if (userModel.UserId > 0)
                {
                    int userNameCount = theratroveDB.Users.Where(e => e.UserName == userModel.UserName && e.UserType == (byte)UserTypeEnum.Admin && e.UserId != userModel.UserId).Count();
                    if (userNameCount > 0)
                    {
                        ModelState.AddModelError("", "Username already exists in database");
                        return View(userModel);
                    }
                    int emailCount = theratroveDB.Users.Where(e => e.Email == userModel.Email && e.UserType == (byte)UserTypeEnum.Admin && e.UserId != userModel.UserId).Count();
                    if (emailCount > 0)
                    {
                        ModelState.AddModelError("", "Email already exists in database");
                        return View(userModel);
                    }
                    var user = theratroveDB.Users.Where(f => f.UserType == (byte)UserTypeEnum.Admin && f.UserId == userModel.UserId).FirstOrDefault();
                    user.UserName = userModel.UserName;
                    user.Password = userModel.Password;
                    user.Email = userModel.Email;
                    user.UserType = (byte)UserTypeEnum.Admin;
                    user.CreateDate = DateTime.Now;
                    user.Active = userModel.Active;
                    theratroveDB.SaveChanges();
                }
                else
                {
                    int userNameCount = theratroveDB.Users.Where(e => e.UserName == userModel.UserName && e.UserType == (byte)UserTypeEnum.Admin).Count();
                    if (userNameCount > 0)
                    {
                        ModelState.AddModelError("", "Username already exists in database");
                        return View(userModel);
                    }
                    int emailCount = theratroveDB.Users.Where(e => e.Email == userModel.Email && e.UserType == (byte)UserTypeEnum.Admin).Count();
                    if (emailCount > 0)
                    {
                        ModelState.AddModelError("", "Email already exists in database");
                        return View(userModel);
                    }
                    User user = new data.User();
                    user.Email = userModel.Email;
                    user.UserName = userModel.UserName;
                    user.Password = userModel.Password;
                    user.CreateDate = DateTime.Now;
                    user.UserType = (byte)UserTypeEnum.Admin;
                    user.Active = userModel.Active;
                    theratroveDB.Users.Add(user);
                    theratroveDB.SaveChanges();
                }
                return RedirectToAction("UserList");
            }
            else
            {
                string messages = string.Join("; ", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
                ModelState.AddModelError("", messages);
            }
            return View(userModel);
        }


        //public ActionResult ViewTherapists()
        //{
        //    CheckLogin();
        //    UserModel userModel = new UserModel();
        //    var theratroveDB = new TheratroveDB();
        //    userModel.UserList = theratroveDB.Users.Where(d => d.UserType == (byte)UserTypeEnum.Therapist).ToList();
        //    return View(userModel);
        //}
        [CustomAuthorization(LoginPage = Constants.AdminLogingURL)]
        public ActionResult ViewTherapists(string sortOrder, string CurrentSort, int? page, string OrderBy)
        {
            var db = new TheratroveDB();
            UserModel userModel = new UserModel();
            int pageSize = 20;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            ViewBag.CurrentSort = sortOrder;
            if (string.IsNullOrEmpty(OrderBy))
            {
                OrderBy = "asc";
            }
            if (pageIndex == 1)
            {
                if (OrderBy == "desc")
                    OrderBy = "asc";
                else
                    OrderBy = "desc";
                ViewBag.OrderBy = OrderBy;
            }
            else
            {
                ViewBag.CurrentSort = sortOrder;
            }
            sortOrder = String.IsNullOrEmpty(sortOrder) ? "ID" : sortOrder;
            IPagedList<User> user = null;
            switch (sortOrder)
            {
                case "ID":
                    if (OrderBy.Equals("desc"))
                        user = db.Users.Where(d => d.UserType == (byte)UserTypeEnum.Therapist).OrderByDescending
                                (m => m.UserId).ToPagedList(pageIndex, pageSize);
                    else
                        user = db.Users.Where(d => d.UserType == (byte)UserTypeEnum.Therapist).OrderBy
                                (m => m.UserId).ToPagedList(pageIndex, pageSize);
                    break;
                case "Username":
                    if (OrderBy.Equals("desc"))
                        user = db.Users.Where(d => d.UserType == (byte)UserTypeEnum.Therapist).OrderByDescending
                                (m => m.UserName).ToPagedList(pageIndex, pageSize);
                    else
                        user = db.Users.Where(d => d.UserType == (byte)UserTypeEnum.Therapist).OrderBy
                                (m => m.UserName).ToPagedList(pageIndex, pageSize);
                    break;
                case "Email":
                    if (OrderBy.Equals("desc"))
                        user = db.Users.Where(d => d.UserType == (byte)UserTypeEnum.Therapist).OrderByDescending
                                (m => m.Email).ToPagedList(pageIndex, pageSize);
                    else
                        user = db.Users.Where(d => d.UserType == (byte)UserTypeEnum.Therapist).OrderBy
                                (m => m.Email).ToPagedList(pageIndex, pageSize);
                    break;
                case "FirstName":
                    if (OrderBy.Equals("desc"))
                        user = db.Users.Where(d => d.UserType == (byte)UserTypeEnum.Therapist).OrderByDescending
                                (m => m.Therapist.FirstName).ToPagedList(pageIndex, pageSize);
                    else
                        user = db.Users.Where(d => d.UserType == (byte)UserTypeEnum.Therapist).OrderBy
                                (m => m.Therapist.FirstName).ToPagedList(pageIndex, pageSize);
                    break;
                case "LastName":
                    if (OrderBy.Equals("desc"))
                        user = db.Users.Where(d => d.UserType == (byte)UserTypeEnum.Therapist).OrderByDescending
                                (m => m.Therapist.LastName).ToPagedList(pageIndex, pageSize);
                    else
                        user = db.Users.Where(d => d.UserType == (byte)UserTypeEnum.Therapist).OrderBy
                                (m => m.Therapist.LastName).ToPagedList(pageIndex, pageSize);
                    break;
                case "YearsInService":
                    if (OrderBy.Equals("desc"))
                        user = db.Users.Where(d => d.UserType == (byte)UserTypeEnum.Therapist).OrderByDescending
                                (m => m.Therapist.YearsInService).ToPagedList(pageIndex, pageSize);
                    else
                        user = db.Users.Where(d => d.UserType == (byte)UserTypeEnum.Therapist).OrderBy
                                (m => m.Therapist.YearsInService).ToPagedList(pageIndex, pageSize);
                    break;
                case "CreateDate":
                    if (OrderBy.Equals("desc"))
                        user = db.Users.Where(d => d.UserType == (byte)UserTypeEnum.Therapist).OrderByDescending
                                (m => m.CreateDate).ToPagedList(pageIndex, pageSize);
                    else
                        user = db.Users.Where(d => d.UserType == (byte)UserTypeEnum.Therapist).OrderBy
                                (m => m.CreateDate).ToPagedList(pageIndex, pageSize);
                    break;
                case "Active":
                    if (OrderBy.Equals("desc"))
                        user = db.Users.Where(d => d.UserType == (byte)UserTypeEnum.Therapist).OrderByDescending
                                (m => m.Active).ToPagedList(pageIndex, pageSize);
                    else
                        user = db.Users.Where(d => d.UserType == (byte)UserTypeEnum.Therapist).OrderBy
                                (m => m.Active).ToPagedList(pageIndex, pageSize);
                    break;
                case "Default":
                    user = db.Users.Where(d => d.UserType == (byte)UserTypeEnum.Therapist).OrderBy
                        (m => m.UserId).ToPagedList(pageIndex, pageSize);
                    break;
            }

            return View(user);
        }
        [CustomAuthorization(LoginPage = Constants.AdminLogingURL)]
        public ActionResult SearchFilterValues(string selectedFilters = "")
        {
            CheckLogin();
            var theratroveDB = new TheratroveDB();

            SearchFilterValueModel filters = new SearchFilterValueModel();

            filters.Filters = theratroveDB.SearchFilters.ToList().ConvertAll(a =>
            {
                return new SelectListItem()
                {
                    Text = a.Name,
                    Value = a.Id.ToString(),
                    Selected = false
                };
            });

            if (filters.Filters.Count > 0 && string.IsNullOrWhiteSpace(selectedFilters))
            {
                filters.Filters[0].Selected = true;
                int selectedId = Convert.ToInt32(filters.Filters[0].Value.ToString());
                ViewBag.SelectedFilterId = selectedId;
                filters.SearchFilterValuesList = theratroveDB.SearchFilterValues.Where(f => f.SearchFilterID == selectedId).OrderBy(d => d.DisplayOrder).ToList();
            }
            else
            {
                int selectedId = Convert.ToInt32(selectedFilters);
                ViewBag.SelectedFilterId = selectedId;
                filters.SearchFilterValuesList = theratroveDB.SearchFilterValues.Where(f => f.SearchFilterID == selectedId).OrderBy(d => d.DisplayOrder).ToList();
            }

            return View(filters);
        }

        [CustomAuthorization(LoginPage = Constants.AdminLogingURL)]
        public ActionResult SearchFilter()
        {
            CheckLogin();
            var theratroveDB = new TheratroveDB();

            SearchFilterModel searchFilterModel = new SearchFilterModel();
            searchFilterModel.SearchFilterList = theratroveDB.SearchFilters.ToList();
            return View(searchFilterModel);
        }

        [CustomAuthorization(LoginPage = Constants.AdminLogingURL)]
        public ActionResult SearchFilterAddEdit(int id)
        {
            CheckLogin();
            var theratroveDB = new TheratroveDB();
            SearchFilterModel searchFilterModel = new SearchFilterModel();
            if (id > 0)
            {
                searchFilterModel.SearchFilter = theratroveDB.SearchFilters.Where(e => e.Id == id).FirstOrDefault();
            }

            searchFilterModel.ControlTypeList.Add(new SelectListItem() { Value = "1", Text = "CheckBox", Selected = ((searchFilterModel.SearchFilter == null ? false : (searchFilterModel.SearchFilter.FilterControlType.Value) == 1)) });
            searchFilterModel.ControlTypeList.Add(new SelectListItem() { Value = "2", Text = "DropDown", Selected = ((searchFilterModel.SearchFilter == null ? false : (searchFilterModel.SearchFilter.FilterControlType.Value) == 2)) });
            ViewBag.StartTimeLoadHours = searchFilterModel.ControlTypeList;

            return View(searchFilterModel);
        }

        [CustomAuthorization(LoginPage = Constants.AdminLogingURL)]
        [HttpPost]
        public ActionResult SearchFilterAddEdit(SearchFilter searchFilter)
        {
            CheckLogin();
            var theratroveDB = new TheratroveDB();
            SearchFilterModel searchFilterModel = new SearchFilterModel();
            searchFilterModel.SearchFilter = searchFilter;
            if (searchFilter.Id == 0)
            {
                ViewBag.ErrorMessage = "You can't create new filter";
                return View(searchFilterModel);
            }
            else
            {
                SearchFilter searchFilterOld = new data.SearchFilter();
                searchFilterOld = theratroveDB.SearchFilters.Where(e => e.Id == searchFilter.Id).FirstOrDefault();
                searchFilterOld.FilterControlType = searchFilter.FilterControlType;
                searchFilterOld.Name = searchFilter.Name;
                searchFilterOld.Active = searchFilter.Active;
                theratroveDB.SaveChanges();
                ApplicationCashManager.filterOptions = null;
                return RedirectToAction("SearchFilter");
            }
        }

        [CustomAuthorization(LoginPage = Constants.AdminLogingURL)]
        public ActionResult SearchFilterValueAddEdit(int id, int selectedFilter)
        {
            CheckLogin();
            var theratroveDB = new TheratroveDB();
            SearchFilterValueModel searchFilterModel = new SearchFilterValueModel();
            searchFilterModel.Filters = theratroveDB.SearchFilters.ToList().ConvertAll(a =>
            {
                return new SelectListItem()
                {
                    Text = a.Name,
                    Value = a.Id.ToString()
                };
            });

            ViewBag.selectedFilter = selectedFilter;

            if (id > 0)
            {
                searchFilterModel.SelectedValue = theratroveDB.SearchFilterValues.Where(e => e.Id == id).FirstOrDefault();
                searchFilterModel.SelectedValue.SearchFilterID = Convert.ToInt32(selectedFilter);
            }
            else
            {
                searchFilterModel.SelectedValue = new SearchFilterValue();
                searchFilterModel.SelectedValue.SearchFilterID = Convert.ToInt32(selectedFilter);
            }
            return View(searchFilterModel);
        }

        [HttpPost]
        public ActionResult SearchFilterValueAddEdit(SearchFilterValue SelectedValue)
        {
            CheckLogin();
            var theratroveDB = new TheratroveDB();

            SearchFilterValueModel searchFilterModel = new SearchFilterValueModel();

            if (SelectedValue.Id == 0)
            {
                var existingCount = theratroveDB.SearchFilterValues.Where(d => d.Name == SelectedValue.Name).Count();

                if (existingCount > 0)
                {
                    ViewBag.selectedFilter = SelectedValue;
                    searchFilterModel.SelectedValue = theratroveDB.SearchFilterValues.Where(e => e.Id == SelectedValue.Id).FirstOrDefault();

                    ViewBag.ErrorMessage = "Same name already exists";
                    return View(searchFilterModel);
                }

                SearchFilterValue searchFilterValue = new data.SearchFilterValue();
                searchFilterValue.Name = SelectedValue.Name;
                searchFilterValue.Value = SelectedValue.Value;
                searchFilterValue.DisplayOrder = SelectedValue.DisplayOrder;
                searchFilterValue.Active = SelectedValue.Active;
                searchFilterValue.CreateDate = DateTime.Today;
                searchFilterValue.SearchFilterID = SelectedValue.SearchFilterID;
                theratroveDB.SearchFilterValues.Add(searchFilterValue);
                ApplicationCashManager.filterOptions = null;
                theratroveDB.SaveChanges();
            }
            else
            {
                var searchFilterValue = theratroveDB.SearchFilterValues.Where(f => f.Id == SelectedValue.Id).FirstOrDefault();

                var existingCount = theratroveDB.SearchFilterValues.Where(d => d.Name == SelectedValue.Name && d.Id != SelectedValue.Id && d.SearchFilterID == SelectedValue.SearchFilterID).Count();

                if (existingCount > 0)
                {
                    ViewBag.selectedFilter = SelectedValue;
                    searchFilterModel.SelectedValue = theratroveDB.SearchFilterValues.Where(e => e.Id == SelectedValue.Id).FirstOrDefault();

                    ViewBag.ErrorMessage = "Same name already exists";
                    return View(ModelState);
                }

                searchFilterValue.Name = SelectedValue.Name;
                searchFilterValue.Value = SelectedValue.Value;
                searchFilterValue.DisplayOrder = SelectedValue.DisplayOrder;
                searchFilterValue.Active = SelectedValue.Active;
                searchFilterValue.CreateDate = DateTime.Today;
                searchFilterValue.SearchFilterID = SelectedValue.SearchFilterID;
                ApplicationCashManager.filterOptions = null;
                theratroveDB.SaveChanges();
            }

            ViewBag.SelectedFilterId = SelectedValue.SearchFilterID;

            return RedirectToAction("SearchFilterValues", new
            {
                selectedFilters = SelectedValue.SearchFilterID
            });
        }

        [CustomAuthorization(LoginPage = Constants.AdminLogingURL)]
        public ActionResult OrderSearchFilter()
        {
            CheckLogin();
            TheratroveDB theratroveDB = new TheratroveDB();
            SearchFilterModel searchFilterModel = new SearchFilterModel();
            searchFilterModel.SearchFilterList = theratroveDB.SearchFilters.OrderBy(e => e.DisplayOrderNo).ToList();
            return View(searchFilterModel);
        }

        [CustomAuthorization(LoginPage = Constants.AdminLogingURL)]
        public ActionResult OrderSearchFilterEdit(int id, string moveFor)
        {
            CheckLogin();
            TheratroveDB theratroveDB = new TheratroveDB();
            SearchFilter searchFilters = theratroveDB.SearchFilters.Where(e => e.Id == id).FirstOrDefault();
            if (moveFor == "Up")
            {
                SearchFilter searchFiltersOld = theratroveDB.SearchFilters.Where(e => e.DisplayOrderNo == searchFilters.DisplayOrderNo - 1).FirstOrDefault();
                searchFiltersOld.DisplayOrderNo = searchFiltersOld.DisplayOrderNo + 1;
                searchFilters.DisplayOrderNo = searchFilters.DisplayOrderNo - 1;

            }
            else if (moveFor == "Down")
            {
                SearchFilter searchFiltersOld = theratroveDB.SearchFilters.Where(e => e.DisplayOrderNo == searchFilters.DisplayOrderNo + 1).FirstOrDefault();
                searchFiltersOld.DisplayOrderNo = searchFiltersOld.DisplayOrderNo - 1;
                searchFilters.DisplayOrderNo = searchFilters.DisplayOrderNo + 1;
            }
            theratroveDB.SaveChanges();

            //SearchFilterModel searchFilterModel = new SearchFilterModel();
            //searchFilterModel.SearchFilterList = theratroveDB.SearchFilters.ToList(); 
            return RedirectToAction("OrderSearchFilter");
        }

        public ActionResult Logout()
        {
            Session.Clear();
            FormsAuthentication.SignOut();

            return RedirectToAction("Login");
        }
        public void CheckLogin()
        {
            //if (string.IsNullOrEmpty(Convert.ToString(Session["UserName"])))
            //{
            //    RedirectToAction("Login", "Admin");
            //}
        }

        [CustomAuthorization(LoginPage = Constants.AdminLogingURL)]
        public ActionResult Therapists(int? id)
        {
            CheckLogin();
            UserModel userModel = new UserModel();
            ViewBag.Message = "Add Therapists";
            TheratroveDB theratroveDB = new TheratroveDB();
            var SearchFilter = theratroveDB.SearchFilterValues.Where(e => e.Active == true).ToList();

            foreach (var item in SearchFilter)
            {
                CheckboxModel checkboxModel = new CheckboxModel();
                checkboxModel.Id = item.Id;
                checkboxModel.Name = item.Name;
                checkboxModel.itemFor = item.SearchFilterID;
                if (item.SearchFilter.FilterType == (byte)FilterTypeEnum.Faith)
                    userModel.SelectedFaith.Add(checkboxModel);
                if (item.SearchFilter.FilterType == (byte)FilterTypeEnum.For)
                    userModel.SelectedFor.Add(checkboxModel);
                if (item.SearchFilter.FilterType == (byte)FilterTypeEnum.GenderPreference)
                    userModel.SelectedGenderPreference.Add(checkboxModel);
                if (item.SearchFilter.FilterType == (byte)FilterTypeEnum.Insurance)
                    userModel.SelectedInsurance.Add(checkboxModel);
                if (item.SearchFilter.FilterType == (byte)FilterTypeEnum.Issues)
                    userModel.SelectedIssues.Add(checkboxModel);
                if (item.SearchFilter.FilterType == (byte)FilterTypeEnum.Language)
                    userModel.SelectedLanguage.Add(checkboxModel);
                if (item.SearchFilter.FilterType == (byte)FilterTypeEnum.Location)
                    userModel.SelectedLocation.Add(checkboxModel);
            }
            var typesOfTherapy = theratroveDB.TherapyTypes.Where(e => e.Active == true).ToList();
            foreach (var item in typesOfTherapy)
            {
                CheckboxModel checkboxModel = new CheckboxModel();
                checkboxModel.Id = item.Id;
                checkboxModel.Name = item.Name;
                userModel.SelectedTherapistTypesOfTherapy.Add(checkboxModel);
            }

            if (id != null && id > 0)
            {
                User user = theratroveDB.Users.Where(e => e.UserId == id).FirstOrDefault();
                userModel.UserId = user.UserId;
                userModel.UserName = user.UserName;
                userModel.Email = user.Email;
                userModel.Password = user.Password;
                userModel.ProfilePic = user.ProfilePicURL;
                userModel.Active = user.Active;
                ViewBag.Message = "Edit Therapists";

                if (user.Therapist != null)
                {
                    userModel.TherapyGalleryList = user.Therapist.TherapyGalleries.ToList();
                    userModel.AboutMe = user.Therapist.AboutMe;
                    userModel.AcceptInsurance = user.Therapist.AcceptInsurance;
                    userModel.Address1 = user.Therapist.Address1;
                    userModel.Address2 = user.Therapist.Address2;
                    userModel.AvgCost = user.Therapist.AvgCost;
                    userModel.City = user.Therapist.City;
                    userModel.Education = user.Therapist.Education;
                    userModel.FirstName = user.Therapist.FirstName;
                    userModel.Gender = user.Therapist.Gender;
                    userModel.LastName = user.Therapist.LastName;
                    userModel.License = user.Therapist.License;
                    userModel.Location = user.Therapist.Location;
                    userModel.PhoneNumber = user.Therapist.PhoneNumber;
                    userModel.State = user.Therapist.State;
                    userModel.Title = user.Therapist.Title;
                    userModel.VideoUrl = user.Therapist.VideoUrl;
                    userModel.YearsInService = user.Therapist.YearsInService;
                    userModel.ZipCode = user.Therapist.ZipCode;

                    var therapistTypesOfTherapiesList = user.Therapist.TherapistTypesOfTherapies.ToList();
                    foreach (var therapistTypesOfTherapie in therapistTypesOfTherapiesList)
                    {
                        var item = userModel.SelectedTherapistTypesOfTherapy.Where(e => e.Id == therapistTypesOfTherapie.TherapyTypesID).FirstOrDefault();
                        if (item != null)
                        {
                            item.Checked = true;
                        }
                    }

                    var therapistExpertisesListFaiths = user.Therapist.TherapistExpertiseFaiths.ToList();
                    foreach (var therapistExpertiseFaiths in therapistExpertisesListFaiths)
                    {
                        var item = userModel.SelectedFaith.Where(e => e.Id == therapistExpertiseFaiths.SearchFilterValueId).FirstOrDefault();
                        if (item != null)
                        {
                            item.Checked = true;
                        }
                    }

                    var therapistExpertisesListFor = user.Therapist.TherapistExpertiseFors.ToList();
                    foreach (var therapistExpertiseFors in therapistExpertisesListFor)
                    {
                        var item = userModel.SelectedFor.Where(e => e.Id == therapistExpertiseFors.SearchFilterValueId).FirstOrDefault();
                        if (item != null)
                        {
                            item.Checked = true;
                        }
                    }


                    var therapistExpertisesListIssues = user.Therapist.TherapistExpertiseIssues.ToList();
                    foreach (var therapistExpertiseIssues in therapistExpertisesListIssues)
                    {
                        var item = userModel.SelectedIssues.Where(e => e.Id == therapistExpertiseIssues.SearchFilterValueId).FirstOrDefault();
                        if (item != null)
                        {
                            item.Checked = true;
                        }
                    }

                    var therapistExpertisesListInsurance = user.Therapist.TherapistExpertiseInsurances.ToList();
                    foreach (var therapistExpertiseInsurance in therapistExpertisesListInsurance)
                    {
                        var item = userModel.SelectedInsurance.Where(e => e.Id == therapistExpertiseInsurance.SearchFilterValueId).FirstOrDefault();
                        if (item != null)
                        {
                            item.Checked = true;
                        }
                    }

                    var therapistExpertisesListGenderPreference = user.Therapist.TherapistExpertiseGenderPreferences.ToList();
                    foreach (var therapistExpertiseGenderPreference in therapistExpertisesListGenderPreference)
                    {
                        var item = userModel.SelectedGenderPreference.Where(e => e.Id == therapistExpertiseGenderPreference.SearchFilterValueId).FirstOrDefault();
                        if (item != null)
                        {
                            item.Checked = true;
                        }
                    }

                    var therapistExpertisesListLanguage = user.Therapist.TherapistExpertiseLanguages.ToList();
                    foreach (var therapistExpertiseLanguage in therapistExpertisesListLanguage)
                    {
                        var item = userModel.SelectedLanguage.Where(e => e.Id == therapistExpertiseLanguage.SearchFilterValueId).FirstOrDefault();
                        if (item != null)
                        {
                            item.Checked = true;
                        }
                    }

                    var therapistExpertisesListLocation = user.Therapist.TherapistExpertiseLocations.ToList();
                    foreach (var therapistExpertiseLocation in therapistExpertisesListLocation)
                    {
                        var item = userModel.SelectedLocation.Where(e => e.Id == therapistExpertiseLocation.SearchFilterValueId).FirstOrDefault();
                        if (item != null)
                        {
                            item.Checked = true;
                        }
                    }

                }
            }

            return View(userModel);
        }

        public ActionResult DeleteTherapists(int? id)
        {
            CheckLogin();
            if (id != null && id > 0)
            {
                TheratroveDB theratroveDB = new TheratroveDB();
                User user = theratroveDB.Users.Where(e => e.UserId == id).FirstOrDefault();
                theratroveDB.Users.Remove(user);
                theratroveDB.SaveChanges();
            }
            return RedirectToAction("ViewTherapists");
        }
        public ActionResult DeleteUser(int? id)
        {
            CheckLogin();
            if (id != null && id > 0)
            {
                TheratroveDB theratroveDB = new TheratroveDB();
                User user = theratroveDB.Users.Where(e => e.UserId == id).FirstOrDefault();
                theratroveDB.Users.Remove(user);
                theratroveDB.SaveChanges();
            }
            return RedirectToAction("UserList");
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult Therapists(UserModel userModel, HttpPostedFileBase fileUpload)
        {
            CheckLogin();
            //UserModel userModel = new UserModel();
            if (ModelState.IsValid)
            {
                TheratroveDB theratroveDB = new TheratroveDB();
                if (userModel.UserId == 0 || userModel.UserId == null)
                {
                    ViewBag.HeaderTitle = "Add Therapists";

                    int userNameCount = theratroveDB.Users.Where(e => e.UserName == userModel.UserName).Count();
                    if (userNameCount > 0)
                    {
                        ModelState.AddModelError("", "Username already exists in database");
                        return View(userModel);
                    }
                    int emailCount = theratroveDB.Users.Where(e => e.Email == userModel.Email && e.UserType == (byte)UserTypeEnum.Therapist).Count();
                    if (emailCount > 0)
                    {
                        ModelState.AddModelError("", "Email already exists in database");
                        return View(userModel);
                    }
                    User user = new data.User();
                    if (fileUpload != null)
                    {
                        string imageURL= theratrove.Infrastructure.Helper.SaveImage(fileUpload, "profile");
                        user.ProfilePicURL = imageURL;
                        //using (Stream inputStream = fileUpload.InputStream)
                        //{
                        //    MemoryStream memoryStream = inputStream as MemoryStream;
                        //    if (memoryStream == null)
                        //    {
                        //        memoryStream = new MemoryStream();
                        //        inputStream.CopyTo(memoryStream);
                        //    }
                        //    user.ProfilePic = memoryStream.ToArray();
                        //}
                    }
                    user.UserName = userModel.UserName;
                    user.Email = userModel.Email;
                    user.Password = userModel.Password;
                    user.CreateDate = DateTime.Now;
                    user.UserType = (byte)UserTypeEnum.Therapist;
                    
                    user.Therapist = new Therapist();
                    user.Therapist.AboutMe = userModel.AboutMe;
                    user.Therapist.AcceptInsurance = userModel.AcceptInsurance;
                    
                    user.Therapist.Address1 = userModel.Address1;
                    user.Therapist.Address2 = userModel.Address2;
                    user.Therapist.AvgCost = userModel.AvgCost;
                    user.Therapist.City = userModel.City;
                    user.Therapist.Education = userModel.Education;
                    user.Therapist.FirstName = userModel.FirstName;
                    user.Therapist.Gender = userModel.Gender;
                    user.Therapist.LastName = userModel.LastName;
                    user.Therapist.License = userModel.License;
                    user.Therapist.Location = userModel.Location;
                    user.Therapist.PhoneNumber = userModel.PhoneNumber;
                    user.Therapist.State = userModel.State;
                    user.Therapist.Title = userModel.Title;
                    user.Therapist.VideoUrl = userModel.VideoUrl;
                    user.Therapist.YearsInService = userModel.YearsInService;
                    user.Therapist.ZipCode = userModel.ZipCode;
                    user.Active = userModel.Active;
                    user.Therapist.Active = userModel.Active;
                    //user.Therapist.TherapistExpertises  =new HashSet<TherapistExpertise>();


                    theratroveDB.TherapistExpertiseFaiths.RemoveRange(user.Therapist.TherapistExpertiseFaiths);
                    theratroveDB.TherapistExpertiseFors.RemoveRange(user.Therapist.TherapistExpertiseFors);
                    theratroveDB.TherapistExpertiseGenderPreferences.RemoveRange(user.Therapist.TherapistExpertiseGenderPreferences);
                    theratroveDB.TherapistExpertiseInsurances.RemoveRange(user.Therapist.TherapistExpertiseInsurances);
                    theratroveDB.TherapistExpertiseIssues.RemoveRange(user.Therapist.TherapistExpertiseIssues);
                    theratroveDB.TherapistExpertiseLanguages.RemoveRange(user.Therapist.TherapistExpertiseLanguages);
                    theratroveDB.TherapistExpertiseLocations.RemoveRange(user.Therapist.TherapistExpertiseLocations);

                    var itemList = userModel.SelectedFaith.Where(e => e.Checked == true);
                    foreach (var item in itemList)
                    {
                        TherapistExpertiseFaith therapistExpertise = new TherapistExpertiseFaith();
                        therapistExpertise.SearchFilterId = item.itemFor;
                        therapistExpertise.SearchFilterValueId = item.Id;
                        user.Therapist.TherapistExpertiseFaiths.Add(therapistExpertise);
                    }
                    var itemListFor = userModel.SelectedFor.Where(e => e.Checked == true);
                    foreach (var item in itemListFor)
                    {
                        TherapistExpertiseFor therapistExpertise = new TherapistExpertiseFor();
                        therapistExpertise.SearchFilterId = item.itemFor;
                        therapistExpertise.SearchFilterValueId = item.Id;
                        user.Therapist.TherapistExpertiseFors.Add(therapistExpertise);
                    }
                    var itemListIssues = userModel.SelectedIssues.Where(e => e.Checked == true);
                    foreach (var item in itemListIssues)
                    {
                        TherapistExpertiseIssue therapistExpertise = new TherapistExpertiseIssue();
                        therapistExpertise.SearchFilterId = item.itemFor;
                        therapistExpertise.SearchFilterValueId = item.Id;
                        user.Therapist.TherapistExpertiseIssues.Add(therapistExpertise);
                    }
                    var itemListInsurance = userModel.SelectedInsurance.Where(e => e.Checked == true);
                    foreach (var item in itemListInsurance)
                    {
                        TherapistExpertiseInsurance therapistExpertise = new TherapistExpertiseInsurance();
                        therapistExpertise.SearchFilterId = item.itemFor;
                        therapistExpertise.SearchFilterValueId = item.Id;
                        user.Therapist.TherapistExpertiseInsurances.Add(therapistExpertise);
                    }
                    var itemListGenderPreference = userModel.SelectedGenderPreference.Where(e => e.Checked == true);
                    foreach (var item in itemListGenderPreference)
                    {
                        TherapistExpertiseGenderPreference therapistExpertise = new TherapistExpertiseGenderPreference();
                        therapistExpertise.SearchFilterId = item.itemFor;
                        therapistExpertise.SearchFilterValueId = item.Id;
                        user.Therapist.TherapistExpertiseGenderPreferences.Add(therapistExpertise);
                    }

                    var itemListLanguage = userModel.SelectedLanguage.Where(e => e.Checked == true);
                    foreach (var item in itemListLanguage)
                    {
                        TherapistExpertiseLanguage therapistExpertise = new TherapistExpertiseLanguage();
                        therapistExpertise.SearchFilterId = item.itemFor;
                        therapistExpertise.SearchFilterValueId = item.Id;
                        user.Therapist.TherapistExpertiseLanguages.Add(therapistExpertise);
                    }

                    var itemListLocation = userModel.SelectedLocation.Where(e => e.Checked == true);
                    foreach (var item in itemListLocation)
                    {
                        TherapistExpertiseLocation therapistExpertise = new TherapistExpertiseLocation();
                        therapistExpertise.SearchFilterId = item.itemFor;
                        therapistExpertise.SearchFilterValueId = item.Id;
                        user.Therapist.TherapistExpertiseLocations.Add(therapistExpertise);
                    }

                    if (user.Therapist.TherapistTypesOfTherapies.Count > 0)
                    {
                        theratroveDB.TherapistTypesOfTherapies.RemoveRange(user.Therapist.TherapistTypesOfTherapies);
                    }

                    var itemTypesOfTherapyList = userModel.SelectedTherapistTypesOfTherapy.Where(e => e.Checked == true);
                    foreach (var item in itemTypesOfTherapyList)
                    {
                        TherapistTypesOfTherapy therapistTypesOfTherapy = new TherapistTypesOfTherapy();
                        therapistTypesOfTherapy.TherapyTypesID = item.Id;
                        user.Therapist.TherapistTypesOfTherapies.Add(therapistTypesOfTherapy);
                    }
                    theratroveDB.Users.Add(user);
                    theratroveDB.SaveChanges();
                    return RedirectToAction("ViewTherapists");
                }
                else
                {
                    ViewBag.HeaderTitle = "Edit Therapists";
                    int userNameCount = theratroveDB.Users.Where(e => e.UserName == userModel.UserName && e.UserId != userModel.UserId).Count();
                    if (userNameCount > 0)
                    {
                        ModelState.AddModelError("", "Username already exists in database");
                        return View(userModel);
                    }
                    int emailCount = theratroveDB.Users.Where(e => e.Email == userModel.Email && e.UserType == (byte)UserTypeEnum.Therapist && e.UserId != userModel.UserId).Count();
                    if (emailCount > 0)
                    {
                        ModelState.AddModelError("", "Email already exists in database");
                        return View(userModel);
                    }
                    User user = theratroveDB.Users.Where(e => e.UserId == userModel.UserId).FirstOrDefault();
                    user.UserName = userModel.UserName;
                    user.Email = userModel.Email;
                    user.Password = userModel.Password;
                    user.CreateDate = DateTime.Now;
                    user.UserType = (byte)UserTypeEnum.Therapist;
                    
                    if (fileUpload != null)
                    {
                        if(!string.IsNullOrEmpty(user.ProfilePicURL))
                        {
                            theratrove.Infrastructure.Helper.DeleteImage(user.ProfilePicURL,"profile");
                        }
                        string imageURL = theratrove.Infrastructure.Helper.SaveImage(fileUpload, "profile");
                        user.ProfilePicURL = imageURL;

                        //using (Stream inputStream = fileUpload.InputStream)
                        //{
                        //    MemoryStream memoryStream = inputStream as MemoryStream;
                        //    if (memoryStream == null)
                        //    {
                        //        memoryStream = new MemoryStream();
                        //        inputStream.CopyTo(memoryStream);
                        //    }
                        //    user.ProfilePic = memoryStream.ToArray();
                        //}
                    }
                    if (user.Therapist == null)
                        user.Therapist = new Therapist();

                    user.Therapist.AboutMe = userModel.AboutMe;
                    user.Therapist.AcceptInsurance = userModel.AcceptInsurance;
                    user.Therapist.Address1 = userModel.Address1;
                    user.Therapist.Address2 = userModel.Address2;
                    user.Therapist.AvgCost = userModel.AvgCost;
                    user.Therapist.City = userModel.City;
                    user.Therapist.Education = userModel.Education;
                    user.Therapist.FirstName = userModel.FirstName;
                    user.Therapist.Gender = userModel.Gender;
                    user.Therapist.LastName = userModel.LastName;
                    user.Therapist.License = userModel.License;
                    user.Therapist.Location = userModel.Location;
                    user.Therapist.PhoneNumber = userModel.PhoneNumber;
                    user.Therapist.State = userModel.State;
                    user.Therapist.Title = userModel.Title;
                    user.Therapist.VideoUrl = userModel.VideoUrl;
                    user.Therapist.YearsInService = userModel.YearsInService;
                    user.Therapist.ZipCode = userModel.ZipCode;
                    user.Active = userModel.Active;
                    user.Therapist.Active = userModel.Active;

                    theratroveDB.TherapistExpertiseFaiths.RemoveRange(user.Therapist.TherapistExpertiseFaiths);
                    theratroveDB.TherapistExpertiseFors.RemoveRange(user.Therapist.TherapistExpertiseFors);
                    theratroveDB.TherapistExpertiseGenderPreferences.RemoveRange(user.Therapist.TherapistExpertiseGenderPreferences);
                    theratroveDB.TherapistExpertiseInsurances.RemoveRange(user.Therapist.TherapistExpertiseInsurances);
                    theratroveDB.TherapistExpertiseIssues.RemoveRange(user.Therapist.TherapistExpertiseIssues);
                    theratroveDB.TherapistExpertiseLanguages.RemoveRange(user.Therapist.TherapistExpertiseLanguages);
                    theratroveDB.TherapistExpertiseLocations.RemoveRange(user.Therapist.TherapistExpertiseLocations);

                    var itemList = userModel.SelectedFaith.Where(e => e.Checked == true);
                    foreach (var item in itemList)
                    {
                        TherapistExpertiseFaith therapistExpertise = new TherapistExpertiseFaith();
                        therapistExpertise.SearchFilterId = item.itemFor;
                        therapistExpertise.SearchFilterValueId = item.Id;
                        user.Therapist.TherapistExpertiseFaiths.Add(therapistExpertise);
                    }
                    var itemListFor = userModel.SelectedFor.Where(e => e.Checked == true);
                    foreach (var item in itemListFor)
                    {
                        TherapistExpertiseFor therapistExpertise = new TherapistExpertiseFor();
                        therapistExpertise.SearchFilterId = item.itemFor;
                        therapistExpertise.SearchFilterValueId = item.Id;
                        user.Therapist.TherapistExpertiseFors.Add(therapistExpertise);
                    }
                    var itemListIssues = userModel.SelectedIssues.Where(e => e.Checked == true);
                    foreach (var item in itemListIssues)
                    {
                        TherapistExpertiseIssue therapistExpertise = new TherapistExpertiseIssue();
                        therapistExpertise.SearchFilterId = item.itemFor;
                        therapistExpertise.SearchFilterValueId = item.Id;
                        user.Therapist.TherapistExpertiseIssues.Add(therapistExpertise);
                    }
                    var itemListInsurance = userModel.SelectedInsurance.Where(e => e.Checked == true);
                    foreach (var item in itemListInsurance)
                    {
                        TherapistExpertiseInsurance therapistExpertise = new TherapistExpertiseInsurance();
                        therapistExpertise.SearchFilterId = item.itemFor;
                        therapistExpertise.SearchFilterValueId = item.Id;
                        user.Therapist.TherapistExpertiseInsurances.Add(therapistExpertise);
                    }
                    var itemListGenderPreference = userModel.SelectedGenderPreference.Where(e => e.Checked == true);
                    foreach (var item in itemListGenderPreference)
                    {
                        TherapistExpertiseGenderPreference therapistExpertise = new TherapistExpertiseGenderPreference();
                        therapistExpertise.SearchFilterId = item.itemFor;
                        therapistExpertise.SearchFilterValueId = item.Id;
                        user.Therapist.TherapistExpertiseGenderPreferences.Add(therapistExpertise);
                    }

                    var itemListLanguage = userModel.SelectedLanguage.Where(e => e.Checked == true);
                    foreach (var item in itemListLanguage)
                    {
                        TherapistExpertiseLanguage therapistExpertise = new TherapistExpertiseLanguage();
                        therapistExpertise.SearchFilterId = item.itemFor;
                        therapistExpertise.SearchFilterValueId = item.Id;
                        user.Therapist.TherapistExpertiseLanguages.Add(therapistExpertise);
                    }

                    var itemListLocation = userModel.SelectedLocation.Where(e => e.Checked == true);
                    foreach (var item in itemListLocation)
                    {
                        TherapistExpertiseLocation therapistExpertise = new TherapistExpertiseLocation();
                        therapistExpertise.SearchFilterId = item.itemFor;
                        therapistExpertise.SearchFilterValueId = item.Id;
                        user.Therapist.TherapistExpertiseLocations.Add(therapistExpertise);
                    }

                    if (user.Therapist.TherapistTypesOfTherapies.Count > 0)
                    {
                        theratroveDB.TherapistTypesOfTherapies.RemoveRange(user.Therapist.TherapistTypesOfTherapies);
                    }

                    var itemTypesOfTherapyList = userModel.SelectedTherapistTypesOfTherapy.Where(e => e.Checked == true);
                    foreach (var item in itemTypesOfTherapyList)
                    {
                        TherapistTypesOfTherapy therapistTypesOfTherapy = new TherapistTypesOfTherapy();
                        therapistTypesOfTherapy.TherapyTypesID = item.Id;
                        user.Therapist.TherapistTypesOfTherapies.Add(therapistTypesOfTherapy);
                    }
                    theratroveDB.SaveChanges();
                    return RedirectToAction("ViewTherapists");
                }


            }
            else
            {
                string messages = string.Join("; ", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
                ModelState.AddModelError("", messages);

            }
            return View(userModel);
        }


        [CustomAuthorization(LoginPage = Constants.AdminLogingURL)]
        public ActionResult UploadBatch()
        {

            return View();
        }
        [HttpPost]
        [CustomAuthorization(LoginPage = Constants.AdminLogingURL)]
        public ActionResult UploadBatch(HttpPostedFileBase FileUpload)
        {


            List<string> data = new List<string>();
            if (FileUpload != null)
            {
                if (FileUpload.ContentType == "application/x-zip-compressed" || FileUpload.ContentType == "pplication/x-zip-compressed" || FileUpload.ContentType == "multipart/x-zip")
                {

                    string fileNameZip = "Import.zip";
                    string fileNameCSV = "Therapis_List.csv";

                    //string filename = FileUpload.FileName;
                    string targetpath = Server.MapPath("~/Doc/Import");
                    string filePath = Path.Combine(Server.MapPath("~/Doc"), fileNameZip);
                    if (System.IO.Directory.Exists(targetpath))
                    {
                        Array.ForEach(Directory.GetFiles(targetpath), delegate (string path) { System.IO.File.Delete(path); });
                        System.IO.DirectoryInfo deleteTheseFiles = new System.IO.DirectoryInfo(targetpath);
                        deleteTheseFiles.Delete(true);
                    }
                    
                    if (System.IO.File.Exists(targetpath + "/" + fileNameCSV))
                    {
                        System.IO.File.Delete(targetpath + "/" + fileNameCSV);
                    }

                    FileUpload.SaveAs(filePath);
                    ZipFile.ExtractToDirectory(filePath, targetpath);


                   

                    string filename = targetpath + "/" + fileNameCSV;
                    //string targetpath = Server.MapPath("~/Doc/");
                    //string filePath = Path.Combine(targetpath, filename);
                    //FileUpload.SaveAs(filePath);
                    string pathToExcelFile = Path.Combine(targetpath, filename);

                    TheratroveDB theratroveDB = new TheratroveDB();
                    var connString = string.Format(
                        @"Provider=Microsoft.Jet.OleDb.4.0; Data Source={0};Extended Properties=""Text;HDR=YES;FMT=Delimited""",
                        Path.GetDirectoryName(pathToExcelFile)
                    );
                    var ds = new DataSet("CSV File");
                    using (var conn = new OleDbConnection(connString))
                    {
                        conn.Open();
                        var query = "SELECT * FROM [" + Path.GetFileName(filename) + "]";
                        using (var adapter = new OleDbDataAdapter(query, conn))
                        {
                            adapter.Fill(ds);
                        }
                    }

                    //int i = ds.Tables[0].Rows.Count;

                    foreach (System.Data.DataRow item in ds.Tables[0].Rows)
                    {
                        string username = item["Username"].ToString();
                        string Email = item["Email"].ToString();
                        string FirstName = item["FirstName"].ToString();
                        string LastName = item["LastName"].ToString();
                        string Title = item["Title"].ToString();
                        //string LocationAddress = item["LocationAddress"].ToString();
                        string City = item["City"].ToString();
                        string State = item["State"].ToString();
                        string ZipCode = item["ZipCode"].ToString();
                        string PhoneNumber = item["PhoneNumber"].ToString();
                        //string LocationCoordinates = item["LocationCoordinates"].ToString();
                        string YearInService = item["YearInService"].ToString();
                        string AvgCost = item["AvgCost"].ToString();
                        string Gender = Convert.ToString(item["Gender"]);
                        string Education = item["Education"].ToString();
                        string AcceptInsurance = item["AcceptInsurance"].ToString();
                        string License = item["License"].ToString();
                        string AboutMe = item["AboutMe"].ToString();
                        string VideoURL = item["VideoURL"].ToString();
                        string TypeOfTherapy = Convert.ToString(item["TypeOfTherapy"]);
                        string Issues = Convert.ToString(item["(s)Issues"]);
                        string Insurance = Convert.ToString(item["(s)Insurance"]);
                        string GenderPreference = Convert.ToString(item["(s)Gender"]);
                        string Language = Convert.ToString(item["(s)Language"]);
                        string Faith = Convert.ToString(item["(s)Faith"]);
                        string LookingFor = Convert.ToString(item["(s)LookingFor"]);
                        //string Location = Convert.ToString(item["(s)Location"]);
                        int emailCount = 0;
                        int userNameCount = 0;
                        //if (!string.IsNullOrEmpty(username))
                        //{
                        //    userNameCount = theratroveDB.Users.Where(e => e.UserName == username).Count();
                        //}
                        //if (!string.IsNullOrEmpty(Email))
                        //{
                        //    emailCount = theratroveDB.Users.Where(e => e.UserName == username).Count();
                        //}
                        if (emailCount == 0 && emailCount == 0)
                        {
                            User user = new theratrove.data.User();
                            user.Active = true;
                            user.UserName = username;
                            user.Email = Email;
                            user.CreateDate = DateTime.Now;
                            user.UserType = (byte)UserTypeEnum.Therapist;
                            user.Therapist = new Therapist();
                            user.Therapist.CreateDate = DateTime.Now;
                            user.Therapist.FirstName = FirstName;
                            user.Therapist.LastName = LastName;
                            user.Therapist.Title = Title;
                            user.Therapist.City = City;
                            user.Therapist.PhoneNumber = PhoneNumber;
                            user.Therapist.State = State;
                            user.Therapist.ZipCode = ZipCode;
                            //user.Therapist.Location = LocationCoordinates;
                            //user.Therapist.Address1 = LocationAddress;
                            user.Therapist.YearsInService = YearInService;
                            if (!string.IsNullOrEmpty(AvgCost))
                                user.Therapist.AvgCost = Convert.ToInt32(AvgCost);

                            if (!string.IsNullOrEmpty(Gender))
                            {
                                if (Gender.ToLower() == "male")
                                    user.Therapist.Gender = 1;
                                else
                                    user.Therapist.Gender = 2;
                            }
                            user.Therapist.Education = Education;
                            if (!string.IsNullOrEmpty(AcceptInsurance))
                                user.Therapist.AcceptInsurance = (AcceptInsurance.ToLower() == "yes" ? true : false);
                            user.Therapist.License = License;
                            user.Therapist.AboutMe = AboutMe;
                            user.Therapist.VideoUrl = VideoURL;
                            theratroveDB.Users.Add(user);

                            AddTherapistExpertise(Issues, theratroveDB, user, (byte)FilterTypeEnum.Issues);
                            AddTherapistExpertise(Insurance, theratroveDB, user, (byte)FilterTypeEnum.Insurance);
                            AddTherapistExpertise(GenderPreference, theratroveDB, user, (byte)FilterTypeEnum.GenderPreference);
                            AddTherapistExpertise(Language, theratroveDB, user, (byte)FilterTypeEnum.Language);
                            AddTherapistExpertise(Faith, theratroveDB, user, (byte)FilterTypeEnum.Faith);
                            AddTherapistExpertise(LookingFor, theratroveDB, user, (byte)FilterTypeEnum.For);
                            //AddTherapistExpertise(Location, theratroveDB, user, (byte)FilterTypeEnum.Location);
                            if (!string.IsNullOrEmpty(TypeOfTherapy))
                            {
                                string[] TypeOfTherapyList = TypeOfTherapy.Split(';');
                                if (TypeOfTherapyList.Count() > 0)
                                {
                                    foreach (string issue in TypeOfTherapyList)
                                    {
                                        TherapyType therapyTypes = theratroveDB.TherapyTypes.Where(e => e.Name.ToLower().Trim() == issue.ToLower().Trim()).FirstOrDefault();
                                        if (therapyTypes != null)
                                        {
                                            TherapistTypesOfTherapy therapistTypesOfTherapy = new TherapistTypesOfTherapy();
                                            therapistTypesOfTherapy.TherapyTypesID = therapyTypes.Id;
                                            user.Therapist.TherapistTypesOfTherapies.Add(therapistTypesOfTherapy);
                                        }
                                    }
                                }
                            }
                            string error = "";
                            try
                            {


                                if (System.IO.File.Exists(Server.MapPath("~/") + @"\Doc\Import\Images\Profile\" + user.UserName + ".jpg"))
                                {
                                    string urlImage = theratrove.Infrastructure.Helper.SaveImageByteArray(System.IO.File.ReadAllBytes(Server.MapPath("~/") + "/Doc/Import/Images/Profile/" + user.UserName + ".jpg"), "therapygallery", user.UserName + ".jpg");
                                    user.ProfilePicURL = urlImage;
                                }

                                if (System.IO.Directory.Exists(Server.MapPath("~/") + @"\Doc/Import/Images/Galleries/" + user.UserName))
                                {
                                    DirectoryInfo d = new DirectoryInfo(Server.MapPath("~/") + @"\Doc/Import/Images/Galleries/" + user.UserName);//Assuming Test is your Folder
                                    FileInfo[] Files = d.GetFiles("*.jpg"); //Getting Text files

                                    foreach (FileInfo file in Files)
                                    {
                                        TherapyGallery therapyGallery = new TherapyGallery();
                                        string urlImage = theratrove.Infrastructure.Helper.SaveImageByteArray(System.IO.File.ReadAllBytes(file.FullName), "therapygallery", file.Name);
                                        therapyGallery.GalleryImageURL = urlImage;
                                        therapyGallery.CreateDateTime = DateTime.Now;
                                        user.TherapyGalleries.Add(therapyGallery);
                                    }
                                }

                                theratroveDB.SaveChanges();
                                ViewBag.Message = "File successfully imported";
                            }
                            catch (DbEntityValidationException e)
                            {
                                foreach (var eve in e.EntityValidationErrors)
                                {
                                    error = error + string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                                    foreach (var ve in eve.ValidationErrors)
                                    {
                                        error = error + string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                            ve.PropertyName, ve.ErrorMessage);
                                    }
                                }

                                ViewBag.ErrorMessage = error;


                            }
                        }
                        else
                        {
                            ViewBag.ErrorMessage = ViewBag.ErrorMessage + "Already exists in database - User Name:" + username + ",  Email:" + Email + "<br>";
                        }

                    }

                }
            }

            return View();
        }



        [CustomAuthorization(LoginPage = Constants.AdminLogingURL)]
        public ActionResult PrivacyPolicy()
        {
            TheratroveDB theratroveDB = new TheratroveDB();
            PageContent pageContent = theratroveDB.PageContents.Where(e => e.PageName == ContentType.PrivacyPolicy.ToString()).FirstOrDefault();
            return View(pageContent);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult PrivacyPolicy(PageContent pageContentVM)
        {
            TheratroveDB theratroveDB = new TheratroveDB();
            var pageContents = theratroveDB.PageContents.Where(e => e.PageName == ContentType.PrivacyPolicy.ToString()).FirstOrDefault();
            if (pageContents == null)
            {
                pageContents = new PageContent();
                pageContents.CreateDate = DateTime.Now;
                pageContents.PageName = ContentType.PrivacyPolicy.ToString();

            }
            pageContents.PageContent1 = pageContentVM.PageContent1;
            pageContents.UpdatedDate = DateTime.Now;
            if (pageContents.Id == 0)
                theratroveDB.PageContents.Add(pageContents);
            theratroveDB.SaveChanges();
            return View(pageContents);
        }

        [CustomAuthorization(LoginPage = Constants.AdminLogingURL)]
        public ActionResult About()
        {
            TheratroveDB theratroveDB = new TheratroveDB();
            PageContent pageContent = theratroveDB.PageContents.Where(e => e.PageName == ContentType.About.ToString()).FirstOrDefault();
            return View(pageContent);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult About(PageContent pageContentVM)
        {
            TheratroveDB theratroveDB = new TheratroveDB();
            var pageContents = theratroveDB.PageContents.Where(e => e.PageName == ContentType.About.ToString()).FirstOrDefault();
            if (pageContents == null)
            {
                pageContents = new PageContent();
                pageContents.CreateDate = DateTime.Now;
                pageContents.PageName = ContentType.About.ToString();

            }
            pageContents.PageContent1 = pageContentVM.PageContent1;
            pageContents.UpdatedDate = DateTime.Now;
            if (pageContents.Id == 0)
                theratroveDB.PageContents.Add(pageContents);
            theratroveDB.SaveChanges();
            return View(pageContents);
        }

        [CustomAuthorization(LoginPage = Constants.AdminLogingURL)]
        public ActionResult HowItWork()
        {
            TheratroveDB theratroveDB = new TheratroveDB();
            PageContent pageContent = theratroveDB.PageContents.Where(e => e.PageName == ContentType.HowItWork.ToString()).FirstOrDefault();
            return View(pageContent);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult HowItWork(PageContent pageContentVM)
        {
            TheratroveDB theratroveDB = new TheratroveDB();
            var pageContents = theratroveDB.PageContents.Where(e => e.PageName == ContentType.HowItWork.ToString()).FirstOrDefault();
            if (pageContents == null)
            {
                pageContents = new PageContent();
                pageContents.CreateDate = DateTime.Now;
                pageContents.PageName = ContentType.HowItWork.ToString();

            }
            pageContents.PageContent1 = pageContentVM.PageContent1;
            pageContents.UpdatedDate = DateTime.Now;
            if (pageContents.Id == 0)
                theratroveDB.PageContents.Add(pageContents);
            theratroveDB.SaveChanges();
            return View(pageContents);
        }


        [CustomAuthorization(LoginPage = Constants.AdminLogingURL)]
        public ActionResult TermsAndConditions()
        {
            TheratroveDB theratroveDB = new TheratroveDB();
            PageContent pageContent = theratroveDB.PageContents.Where(e => e.PageName == ContentType.TermsAndConditions.ToString()).FirstOrDefault();
            return View(pageContent);
        }

        [HttpPost, ValidateInput(false)]
        [CustomAuthorization(LoginPage = Constants.AdminLogingURL)]
        public ActionResult TermsAndConditions(PageContent pageContentVM)
        {
            TheratroveDB theratroveDB = new TheratroveDB();
            var pageContents = theratroveDB.PageContents.Where(e => e.PageName == ContentType.TermsAndConditions.ToString()).FirstOrDefault();
            if (pageContents == null)
            {
                pageContents = new PageContent();
                pageContents.CreateDate = DateTime.Now;
                pageContents.PageName = ContentType.TermsAndConditions.ToString();

            }
            pageContents.PageContent1 = pageContentVM.PageContent1;
            pageContents.UpdatedDate = DateTime.Now;
            if (pageContents.Id == 0)
                theratroveDB.PageContents.Add(pageContents);
            theratroveDB.SaveChanges();
            return View(pageContents);
        }

        private void AddTherapistExpertise(string itemList, TheratroveDB theratroveDB, User user, Byte filterTypeEnum)
        {
            if (!string.IsNullOrEmpty(itemList))
            {
                string[] GenderList = itemList.Split(';');
                if (GenderList.Count() > 0)
                {
                    foreach (string issue in GenderList)
                    {
                        SearchFilterValue searchFilterValues = theratroveDB.SearchFilterValues.Where(e => e.Name.ToLower().Trim() == issue.ToLower().Trim()).FirstOrDefault();
                        if (searchFilterValues != null)
                        {
                            if (filterTypeEnum == (byte)FilterTypeEnum.Faith)
                            {
                                TherapistExpertiseFaith therapistExpertise = new TherapistExpertiseFaith();
                                therapistExpertise.SearchFilterId = searchFilterValues.SearchFilterID;
                                therapistExpertise.SearchFilterValueId = searchFilterValues.Id;
                                user.Therapist.TherapistExpertiseFaiths.Add(therapistExpertise);
                            }
                            else if (filterTypeEnum == (byte)FilterTypeEnum.For)
                            {
                                TherapistExpertiseFor therapistExpertise = new TherapistExpertiseFor();
                                therapistExpertise.SearchFilterId = searchFilterValues.SearchFilterID;
                                therapistExpertise.SearchFilterValueId = searchFilterValues.Id;
                                user.Therapist.TherapistExpertiseFors.Add(therapistExpertise);
                            }
                            else if (filterTypeEnum == (byte)FilterTypeEnum.GenderPreference)
                            {
                                TherapistExpertiseGenderPreference therapistExpertise = new TherapistExpertiseGenderPreference();
                                therapistExpertise.SearchFilterId = searchFilterValues.SearchFilterID;
                                therapistExpertise.SearchFilterValueId = searchFilterValues.Id;
                                user.Therapist.TherapistExpertiseGenderPreferences.Add(therapistExpertise);
                            }
                            else if (filterTypeEnum == (byte)FilterTypeEnum.Insurance)
                            {
                                TherapistExpertiseInsurance therapistExpertise = new TherapistExpertiseInsurance();
                                therapistExpertise.SearchFilterId = searchFilterValues.SearchFilterID;
                                therapistExpertise.SearchFilterValueId = searchFilterValues.Id;
                                user.Therapist.TherapistExpertiseInsurances.Add(therapistExpertise);
                            }
                            else if (filterTypeEnum == (byte)FilterTypeEnum.Issues)
                            {
                                TherapistExpertiseIssue therapistExpertise = new TherapistExpertiseIssue();
                                therapistExpertise.SearchFilterId = searchFilterValues.SearchFilterID;
                                therapistExpertise.SearchFilterValueId = searchFilterValues.Id;
                                user.Therapist.TherapistExpertiseIssues.Add(therapistExpertise);
                            }
                            else if (filterTypeEnum == (byte)FilterTypeEnum.Language)
                            {
                                TherapistExpertiseLanguage therapistExpertise = new TherapistExpertiseLanguage();
                                therapistExpertise.SearchFilterId = searchFilterValues.SearchFilterID;
                                therapistExpertise.SearchFilterValueId = searchFilterValues.Id;
                                user.Therapist.TherapistExpertiseLanguages.Add(therapistExpertise);
                            }
                            else if (filterTypeEnum == (byte)FilterTypeEnum.Location)
                            {
                                TherapistExpertiseLocation therapistExpertise = new TherapistExpertiseLocation();
                                therapistExpertise.SearchFilterId = searchFilterValues.SearchFilterID;
                                therapistExpertise.SearchFilterValueId = searchFilterValues.Id;
                                user.Therapist.TherapistExpertiseLocations.Add(therapistExpertise);
                            }
                        }
                    }
                }
            }
        }

        [CustomAuthorization(LoginPage = Constants.AdminLogingURL)]
        public ActionResult SearchLogReport()
        {
            var theratroveDB = new TheratroveDB();
            List<SearchFilterLog> searchFilterLogList = theratroveDB.SearchFilterLogs.OrderByDescending(e => e.Id).ToList();
            SearchLogReportModel searchLogReportModel = new SearchLogReportModel();
            searchLogReportModel.SearchFilterLogList = searchFilterLogList;
            return View(searchLogReportModel);
        }
        [CustomAuthorization(LoginPage = Constants.AdminLogingURL)]
        public ActionResult SearchLogReportExport()
        {
            var theratroveDB = new TheratroveDB();
            List<SearchFilterLog> searchFilterLogList = theratroveDB.SearchFilterLogs.OrderByDescending(e => e.Id).ToList();

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("IP, DateCreated, Keyword, City, State,Country, Zip,[s]Issues,[s]Insurance,[s]Gender,[s]Language,[s]Faith,[s]LookingFor,[s]Location" + Environment.NewLine);
            foreach (SearchFilterLog searchFilterLog in searchFilterLogList)
            {
                sb.Append(searchFilterLog.IPAddress + ", " + searchFilterLog.DateCreated.ToString() + ", " + searchFilterLog.Keyword + ", " + searchFilterLog.City + ", " + searchFilterLog.State
                    + ", " + searchFilterLog.Country
                    + ", " + searchFilterLog.Zip + ",  " + searchFilterLog.Issues + "," + searchFilterLog.Insurance + "," + searchFilterLog.Gender + "," + searchFilterLog.Language + "," + searchFilterLog.Faith + "," + searchFilterLog.LookingFor + "," + searchFilterLog.Location + Environment.NewLine);
            }

            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(sb.ToString());
            return File(bytes, "text/csv", "reportlog.csv");
        }
        public ActionResult GalleryPicUpload(int userId, HttpPostedFileBase fileUpload)
        {
            var theratroveDB = new TheratroveDB();

            User user = theratroveDB.Users.Where(e => e.UserId == userId).FirstOrDefault();
            TherapyGallery therapyGallery = new TherapyGallery();
            string urlImage = theratrove.Infrastructure.Helper.SaveImage(fileUpload, "therapygallery");
            therapyGallery.GalleryImageURL = urlImage;
            therapyGallery.CreateDateTime = DateTime.Now;
            user.TherapyGalleries.Add(therapyGallery);
            theratroveDB.SaveChanges();
            //return RedirectToAction("Therapists", "admin", new { userId = userId });
            return RedirectToAction("Therapists", new RouteValueDictionary(new { Id = userId }));
        }
        public ActionResult DeleteGalleryPic(int Id, int userId)
        {
            var theratroveDB = new TheratroveDB();

            TherapyGallery therapyGallery = theratroveDB.TherapyGalleries.Where(e => e.Id == Id).FirstOrDefault();
            if (therapyGallery != null)
            {
                theratrove.Infrastructure.Helper.DeleteImage(therapyGallery.GalleryImageURL, "therapygallery");
                theratroveDB.TherapyGalleries.Remove(therapyGallery);
                theratroveDB.SaveChanges();
            }

            return RedirectToAction("Therapists", new RouteValueDictionary(new { Id = userId }));
        }


        [CustomAuthorization(LoginPage = Constants.AdminLogingURL)]
        public ActionResult TestimonialList()
        {
            CheckLogin();
            TestimonialModel userModel = new TestimonialModel();
            var theratroveDB = new TheratroveDB();
            userModel.TestimonialList = theratroveDB.Testimonials.ToList();
            return View(userModel);
        }
        [CustomAuthorization(LoginPage = Constants.AdminLogingURL)]
        public ActionResult Testimonial(int? Id)
        {
            CheckLogin();
            TestimonialModel userModel = new TestimonialModel();
            if (Id > 0)
            {
                ViewBag.Message = "Edit Testimonial ";
                var theratroveDB = new TheratroveDB();
                var user = theratroveDB.Testimonials.Where(f => f.Id == Id).FirstOrDefault();
                userModel.Testimonial = user;
            }
            else
            {
                ViewBag.Message = "Create Testimonial ";
            }
            return View(userModel);
        }
        [HttpPost]
        [CustomAuthorization(LoginPage = Constants.AdminLogingURL)]
        public ActionResult Testimonial(TestimonialModel testimonialModel)
        {
            if (ModelState.IsValid)
            {
                var theratroveDB = new TheratroveDB();
                int Id = testimonialModel.Testimonial.Id;
                Testimonial testimonial = new Testimonial();
                if (Id == 0)
                {
                    testimonial = new data.Testimonial();
                    testimonial.CreateDate = DateTime.Now;

                }
                else
                {
                    testimonial = theratroveDB.Testimonials.Where(e => e.Id == Id).FirstOrDefault();
                }
                testimonial.Name = testimonialModel.Testimonial.Name;
                testimonial.Active = testimonialModel.Testimonial.Active;
                testimonial.Designation = testimonialModel.Testimonial.Designation;
                testimonial.Details = testimonialModel.Testimonial.Details;
                if (testimonial.Id == 0)
                    theratroveDB.Testimonials.Add(testimonial);
                theratroveDB.SaveChanges();

            }
            else
            {
                string messages = string.Join("; ", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
                ModelState.AddModelError("", messages);
                return View(testimonialModel);
            }
            return RedirectToAction("TestimonialList");
        }

        [CustomAuthorization(LoginPage = Constants.AdminLogingURL)]
        public ActionResult DeleteTestimonial(int? id)
        {
            CheckLogin();
            if (id != null && id > 0)
            {
                TheratroveDB theratroveDB = new TheratroveDB();
                Testimonial testimonial = theratroveDB.Testimonials.Where(e => e.Id == id).FirstOrDefault();
                theratroveDB.Testimonials.Remove(testimonial);
                theratroveDB.SaveChanges();
            }
            return RedirectToAction("TestimonialList");
        }

        [CustomAuthorization(LoginPage = Constants.AdminLogingURL)]
        public ActionResult Widgets()
        {
            CheckLogin();
            WidgetsMode userModel = new WidgetsMode();
            var theratroveDB = new TheratroveDB();

            var Query = (from p in theratroveDB.Therapists.Where(e => e.City != null).GroupBy(p => p.City)
                         select new
                         {
                             CityCount = p.Count(),
                             p.FirstOrDefault().City,
                         }).ToList();
            userModel.WidgetCity = new List<Widget1>();
            foreach (var item in Query)
            {
                userModel.WidgetCity.Add(new Widget1 { City = item.City, CityCount = item.CityCount });
            }
            userModel.TotalSearches = theratroveDB.SearchFilterLogs.Count();
            return View(userModel);
        }


        [HttpGet]
        public ActionResult UploadBatchFile()
        {
            string errorMessage = "File successfully imported";
            string fileNameZip = "Import.zip";
            string fileNameCSV = "Therapis_List.csv";

            //string filename = FileUpload.FileName;
            string targetpath = Server.MapPath("~/Doc/Import");
            string filePath = Path.Combine(Server.MapPath("~/Doc"), fileNameZip);

            Array.ForEach(Directory.GetFiles(targetpath), delegate (string path) { System.IO.File.Delete(path); });
            
            System.IO.DirectoryInfo deleteTheseFiles = new System.IO.DirectoryInfo(targetpath);

            deleteTheseFiles.Delete(true);

            if (System.IO.File.Exists(targetpath + "/" + fileNameCSV))
            {
                System.IO.File.Delete(targetpath + "/" + fileNameCSV);
            }

            ZipFile.ExtractToDirectory(filePath, targetpath);


            List<string> data = new List<string>();
            //if (FileUpload != null)
            {
                // tdata.ExecuteCommand("truncate table OtherCompanyAssets");  
                //if (FileUpload.ContentType == "text/csv" || FileUpload.ContentType == "application/vnd.ms-excel" || FileUpload.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                {

                    //FileUpload.SaveAs(filePath);
                    string pathToExcelFile = Path.Combine(targetpath, fileNameCSV);

                    TheratroveDB theratroveDB = new TheratroveDB();
                    var connString = string.Format(
                        @"Provider=Microsoft.Jet.OleDb.4.0; Data Source={0};Extended Properties=""Text;HDR=YES;FMT=Delimited""",
                        Path.GetDirectoryName(pathToExcelFile)
                    );
                    var ds = new DataSet("CSV File");
                    using (var conn = new OleDbConnection(connString))
                    {
                        conn.Open();
                        var query = "SELECT * FROM [" + Path.GetFileName(fileNameCSV) + "]";
                        using (var adapter = new OleDbDataAdapter(query, conn))
                        {
                            adapter.Fill(ds);
                        }
                    }

                    //int i = ds.Tables[0].Rows.Count;

                    foreach (System.Data.DataRow item in ds.Tables[0].Rows)
                    {
                        string username = item["Username"].ToString();
                        string Email = item["Email"].ToString();
                        string FirstName = item["FirstName"].ToString();
                        string LastName = item["LastName"].ToString();
                        string Title = item["Title"].ToString();
                        //string LocationAddress = item["LocationAddress"].ToString();
                        string City = item["City"].ToString();
                        string State = item["State"].ToString();
                        string ZipCode = item["ZipCode"].ToString();
                        string PhoneNumber = item["PhoneNumber"].ToString();
                        //string LocationCoordinates = item["LocationCoordinates"].ToString();
                        string YearInService = item["YearInService"].ToString();
                        string AvgCost = item["AvgCost"].ToString();
                        string Gender = Convert.ToString(item["Gender"]);
                        string Education = item["Education"].ToString();
                        string AcceptInsurance = item["AcceptInsurance"].ToString();
                        string License = item["License"].ToString();
                        string AboutMe = item["AboutMe"].ToString();
                        string VideoURL = item["VideoURL"].ToString();
                        string TypeOfTherapy = Convert.ToString(item["TypeOfTherapy"]);
                        string Issues = Convert.ToString(item["(s)Issues"]);
                        string Insurance = Convert.ToString(item["(s)Insurance"]);
                        string GenderPreference = Convert.ToString(item["(s)Gender"]);
                        string Language = Convert.ToString(item["(s)Language"]);
                        string Faith = Convert.ToString(item["(s)Faith"]);
                        string LookingFor = Convert.ToString(item["(s)LookingFor"]);
                        //string Location = Convert.ToString(item["(s)Location"]);
                        int emailCount = 0;
                        int userNameCount = 0;
                        //if (!string.IsNullOrEmpty(username))
                        //{
                        //    userNameCount = theratroveDB.Users.Where(e => e.UserName == username).Count();
                        //}
                        //if (!string.IsNullOrEmpty(Email))
                        //{
                        //    emailCount = theratroveDB.Users.Where(e => e.UserName == username).Count();
                        //}
                        if (emailCount == 0 && emailCount == 0)
                        {
                            User user = new theratrove.data.User();
                            user.Active = true;
                            user.UserName = username;
                            user.Email = Email;
                            user.CreateDate = DateTime.Now;
                            user.UserType = (byte)UserTypeEnum.Therapist;
                            user.Therapist = new Therapist();
                            user.Therapist.CreateDate = DateTime.Now;
                            user.Therapist.Active = true;
                            user.Therapist.FirstName = FirstName;
                            user.Therapist.LastName = LastName;
                            user.Therapist.Title = Title;
                            user.Therapist.City = City;
                            user.Therapist.PhoneNumber = PhoneNumber;
                            user.Therapist.State = State;
                            user.Therapist.ZipCode = ZipCode;
                            //user.Therapist.Location = LocationCoordinates;
                            //user.Therapist.Address1 = LocationAddress;
                            user.Therapist.YearsInService = YearInService;
                            if (!string.IsNullOrEmpty(AvgCost))
                                user.Therapist.AvgCost = Convert.ToInt32(AvgCost);

                            if (!string.IsNullOrEmpty(Gender))
                            {
                                if (Gender.ToLower() == "male")
                                    user.Therapist.Gender = 1;
                                else
                                    user.Therapist.Gender = 2;
                            }
                            user.Therapist.Education = Education;
                            if (!string.IsNullOrEmpty(AcceptInsurance))
                                user.Therapist.AcceptInsurance = (AcceptInsurance.ToLower() == "yes" ? true : false);
                            user.Therapist.License = License;
                            user.Therapist.AboutMe = AboutMe;
                            user.Therapist.VideoUrl = VideoURL;
                            theratroveDB.Users.Add(user);

                            AddTherapistExpertise(Issues, theratroveDB, user, (byte)FilterTypeEnum.Issues);
                            AddTherapistExpertise(Insurance, theratroveDB, user, (byte)FilterTypeEnum.Insurance);
                            AddTherapistExpertise(GenderPreference, theratroveDB, user, (byte)FilterTypeEnum.GenderPreference);
                            AddTherapistExpertise(Language, theratroveDB, user, (byte)FilterTypeEnum.Language);
                            AddTherapistExpertise(Faith, theratroveDB, user, (byte)FilterTypeEnum.Faith);
                            AddTherapistExpertise(LookingFor, theratroveDB, user, (byte)FilterTypeEnum.For);
                            //AddTherapistExpertise(Location, theratroveDB, user, (byte)FilterTypeEnum.Location);
                            if (!string.IsNullOrEmpty(TypeOfTherapy))
                            {
                                string[] TypeOfTherapyList = TypeOfTherapy.Split(';');
                                if (TypeOfTherapyList.Count() > 0)
                                {
                                    foreach (string issue in TypeOfTherapyList)
                                    {
                                        TherapyType therapyTypes = theratroveDB.TherapyTypes.Where(e => e.Name.ToLower().Trim() == issue.ToLower().Trim()).FirstOrDefault();
                                        if (therapyTypes != null)
                                        {
                                            TherapistTypesOfTherapy therapistTypesOfTherapy = new TherapistTypesOfTherapy();
                                            therapistTypesOfTherapy.TherapyTypesID = therapyTypes.Id;
                                            user.Therapist.TherapistTypesOfTherapies.Add(therapistTypesOfTherapy);
                                        }
                                    }
                                }
                            }
                            string error = "";
                            try
                            {


                                if (System.IO.File.Exists(Server.MapPath("~/") + @"Doc\Import\Images\Profile\" + user.UserName + ".jpg"))
                                {
                                    string urlImage = theratrove.Infrastructure.Helper.SaveImageByteArray(System.IO.File.ReadAllBytes(Server.MapPath("~/") + "/Doc/Import/Images/Profile/" + user.UserName + ".jpg"), "therapygallery", user.UserName + ".jpg");
                                    user.ProfilePicURL = urlImage;
                                }

                                if (System.IO.Directory.Exists(Server.MapPath("~/") + @"Doc/Import/Images/Galleries/" + user.UserName))
                                {
                                    DirectoryInfo d = new DirectoryInfo(Server.MapPath("~/") + @"\Doc/Import/Images/Galleries/" + user.UserName);//Assuming Test is your Folder
                                    FileInfo[] Files = d.GetFiles("*.jpg"); //Getting Text files

                                    foreach (FileInfo file in Files)
                                    {
                                        TherapyGallery therapyGallery = new TherapyGallery();
                                        string urlImage = theratrove.Infrastructure.Helper.SaveImageByteArray(System.IO.File.ReadAllBytes(file.FullName), "therapygallery", file.Name);
                                        therapyGallery.GalleryImageURL = urlImage;
                                        therapyGallery.CreateDateTime = DateTime.Now;
                                        user.TherapyGalleries.Add(therapyGallery);
                                    }
                                }

                                theratroveDB.SaveChanges();
                            }
                            catch (DbEntityValidationException e)
                            {
                                foreach (var eve in e.EntityValidationErrors)
                                {
                                    error = error + string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                                    foreach (var ve in eve.ValidationErrors)
                                    {
                                        error = error + string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                            ve.PropertyName, ve.ErrorMessage);
                                    }
                                }

                                errorMessage = error;

                            }
                        }
                        else
                        {
                            errorMessage = errorMessage + "Already exists in database - User Name:" + username + ",  Email:" + Email + "<br>";
                        }

                    }

                }
            }

            return Json(new { message = errorMessage }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetChartData()
        {
            TheratroveDB theratroveDB = new TheratroveDB();
            var searchByMonth = theratroveDB.Database.SqlQuery<Widget2>("SP_SearchByMonth").ToList<Widget2>();
            
            var dt = new VisualizationDataTable();
            dt.AddColumn("MonthName", "string");
            dt.AddColumn("Count", "number");

            foreach (var item in searchByMonth)
            {
                dt.NewRow(item.Name, item.Count);
            }
            
            var chart = new ChartViewModel
            {
                Title = "Search By Month",
                Subtitle = "",
                DataTable = dt
            };

            return Content(JsonConvert.SerializeObject(chart), "application/json");
        }

        public ActionResult GetChartDataSearchByCity()
        {
            TheratroveDB theratroveDB = new TheratroveDB();
            
            var searchByCity = theratroveDB.Database.SqlQuery<Widget2>("SP_SearchByCity").ToList<Widget2>();
 
            var dt = new VisualizationDataTable();
            dt.AddColumn("Month Name", "string");
            dt.AddColumn("Count", "number");

            foreach (var item in searchByCity)
            {
                dt.NewRow(item.Name, item.Count);
            }

            var chart = new ChartViewModel
            {
                Title = "Search By City",
                Subtitle = "",
                DataTable = dt
            };

            return Content(JsonConvert.SerializeObject(chart), "application/json");
        }
        public ActionResult GetChartDataSearchByCondition()
        {
            TheratroveDB theratroveDB = new TheratroveDB();

            var searchByCondition = theratroveDB.Database.SqlQuery<Widget2>("SP_SearchByCondition").ToList<Widget2>();

            var dt = new VisualizationDataTable();
            dt.AddColumn("Month Name", "string");
            dt.AddColumn("Count", "number");

            foreach (var item in searchByCondition)
            {
                dt.NewRow(item.Name, item.Count);
            }

            var chart = new ChartViewModel
            {
                Title = "Search By Condition",
                Subtitle = "",
                DataTable = dt
            };

            return Content(JsonConvert.SerializeObject(chart), "application/json");
        }
    }



}
