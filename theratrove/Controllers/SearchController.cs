﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web.Mvc;
using System.Xml;
using theratrove.data;
using theratrove.Helper;
using theratrove.Models;

namespace theratrove.Controllers
{
    public class SearchController : Controller
    {
        // GET: Search
        private int pageSize = 10;
        private TheratroveDB db = new TheratroveDB();
        private int maxLocationValue;

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Therapist(int? Id)
        {
            var result = db.Therapists.Where(t => t.Id == Id && t.Active == true).FirstOrDefault();
            var ratingList = result.TherapistRatings.ToList();
            ViewBag.TherapistRating = Math.Round(ratingList.Average(e => e.Rating).GetValueOrDefault(0), 2);
            ViewBag.TherapistCount = ratingList.Count();

            return View(result);
        }

        public ActionResult Condition(string Name, int pageNo = 1)
        {
            var searchResults = new theratrove.Models.SearchResults();

            this.FillSearchFilters(searchResults);

            searchResults.TherapistList = (from pd in db.Therapists
                                           join od in db.TherapistTypesOfTherapies on pd.Id equals od.TherapistID
                                           where od.TherapyType.Value == Name && pd.Active == true
                                           orderby pd.FirstName
                                           select pd).ToList();

            searchResults.CurrentPage = pageNo;

            if (searchResults.TherapistList != null && searchResults.TherapistList.Count > 0)
            {
                searchResults.NoOfRecords = searchResults.TherapistList.Count;

                if (pageNo == 1 && searchResults.TherapistList.Count > (pageNo * pageSize))
                {
                    searchResults.RecordsRange = string.Format("{0}-{1} of {2}", 1, (pageNo * pageSize), searchResults.NoOfRecords);
                }
                else if ((pageNo * pageSize) >= searchResults.NoOfRecords)
                {
                    if (pageNo == 1)
                    {
                        searchResults.RecordsRange = string.Format("{0} of {1}", searchResults.NoOfRecords, searchResults.NoOfRecords);
                    }
                    else
                    {
                        searchResults.RecordsRange = string.Format("{0} of {1}", ((pageNo - 1) * (pageSize)), searchResults.NoOfRecords);
                    }
                }
                else
                {
                    searchResults.RecordsRange = string.Format("{0}-{1} of {2}", ((pageNo - 1) * pageSize), (pageNo * pageSize), searchResults.NoOfRecords);
                }

                searchResults.TherapistList = searchResults.TherapistList.Skip((pageNo - 1) * pageSize)
                // Take only what is being asked for.
                .Take(pageSize)
                // NOW pull it from the database.
                .ToList();
                this.SetRatting(searchResults);
                this.LogSearchFilters(searchResults, "", "", Name);
                return View("Therapistlist", searchResults);
            }
            else
            {
                return View("Therapistlist");
            }
        }

        public ActionResult Location(string Name, int pageNo = 1)
        {
            var searchResults = new theratrove.Models.SearchResults();

            this.FillSearchFilters(searchResults);
            searchResults.location = Name;
            searchResults.TherapistList = db.Therapists.Where(th => th.City.Equals(Name) && th.Active == true).OrderBy(f => f.FirstName).ToList();
            searchResults.CurrentPage = pageNo;

            if (searchResults.TherapistList != null && searchResults.TherapistList.Count > 0)
            {
                searchResults.NoOfRecords = searchResults.TherapistList.Count;
                if (pageNo == 1 && searchResults.TherapistList.Count > (pageNo * pageSize))
                {
                    searchResults.RecordsRange = string.Format("{0}-{1} of {2}", 1, (pageNo * pageSize), searchResults.NoOfRecords);
                }
                else if ((pageNo * pageSize) >= searchResults.NoOfRecords)
                {
                    if (pageNo == 1)
                    {
                        searchResults.RecordsRange = string.Format("{0} of {1}", searchResults.NoOfRecords, searchResults.NoOfRecords);
                    }
                    else
                    {
                        searchResults.RecordsRange = string.Format("{0} of {1}", ((pageNo - 1) * (pageSize)), searchResults.NoOfRecords);
                    }
                }
                else
                {
                    searchResults.RecordsRange = string.Format("{0}-{1} of {2}", ((pageNo - 1) * pageSize), (pageNo * pageSize), searchResults.NoOfRecords);
                }

                searchResults.TherapistList = searchResults.TherapistList.Skip((pageNo - 1) * pageSize)
                // Take only what is being asked for.
                .Take(pageSize)
                // NOW pull it from the database.
                .ToList();
                this.SetRatting(searchResults);
                this.LogSearchFilters(searchResults, Name, "");
                searchResults.ActivePage = pageNo;
                return View("Therapistlist", searchResults);
            }
            else
            {
                return View("Therapistlist");
            }
        }

        public ActionResult Therapistlist(int pageNo = 1)
        {
            SearchResults searchResults = new SearchResults();

            this.FillSearchFilters(searchResults);

            searchResults.TherapistList = db.Therapists.Where(e => e.Active == true).OrderBy(f => f.FirstName).ToList();
            searchResults.CurrentPage = pageNo;

            if (searchResults.TherapistList != null && searchResults.TherapistList.Count > 0)
            {
                searchResults.NoOfRecords = searchResults.TherapistList.Count;
                if (pageNo == 1 && searchResults.TherapistList.Count > (pageNo * pageSize))
                {
                    searchResults.RecordsRange = string.Format("{0}-{1} of {2}", 1, (pageNo * pageSize), searchResults.TherapistList.Count);
                }
                else if (((pageNo + 1) * pageSize) >= searchResults.TherapistList.Count || (pageNo * pageSize) <= searchResults.TherapistList.Count)
                {
                    searchResults.RecordsRange = string.Format("{0} of {1}", searchResults.TherapistList.Count, searchResults.TherapistList.Count);
                }
                else
                {
                    searchResults.RecordsRange = string.Format("{0}-{1} of {2}", ((pageNo - 1) * pageSize), (pageNo * pageSize), searchResults.TherapistList.Count);
                }

                searchResults.TherapistList = searchResults.TherapistList.Skip((pageNo - 1) * pageSize)
                // Take only what is being asked for.
                .Take(pageSize)
                // NOW pull it from the database.
                .ToList();
                this.SetRatting(searchResults);
            }

            //return PartialView("_TherapistList", searchResults);
            ViewBag.FilterPara = null;
            searchResults.ActivePage = pageNo;
            return View("Therapistlist", searchResults);
            //return View("TherapistlistTest", searchResults);
        }

        [HttpPost]
        public ActionResult FilterTherapist(SearchResults searchResults, string keyworksearch, string location, int pageNo = 1)
        {
            TempData.Clear();
            TempData.Add("searchResults", searchResults);
            TempData.Add("keyworksearch", keyworksearch);
            TempData.Add("location", location);
            TempData.Add("pageNo", pageNo);

            this.FillSearchFilters(searchResults);
            searchResults.location = location;
            searchResults.keyword = keyworksearch;
            this.SearchTherapist(searchResults, keyworksearch, location, pageNo);
            searchResults.ActivePage = pageNo;
            this.SetRatting(searchResults);
            this.LogSearchFilters(searchResults, location, keyworksearch);
            return View("Therapistlist", searchResults);
            //return PartialView("_TherapistList", searchResults);
        }

        //[HttpGet]
        //public ActionResult FilterTherapist()
        //{
        //    object searchResults;
        //    string keyworksearch, location;
        //    int pageNo;
        //    if (TempData.ContainsKey("searchResults"))
        //    {
        //        searchResults = TempData["searchResults"];
        //    }
        //    if (TempData.ContainsKey("keyworksearch"))
        //    {
        //        keyworksearch = Convert.ToString(TempData["keyworksearch"]);
        //    }
        //    if (TempData.ContainsKey("location"))
        //    {
        //        location = Convert.ToString(TempData["location"]);
        //    }
        //    if (TempData.ContainsKey("pageNo"))
        //    {
        //        pageNo = Convert.ToInt32(TempData["pageNo"]);
        //    }

        //    //return RedirectToAction("Therapistlist", { searchResults, keyworksearch,location,pageNo });
        //}

        private void FillSearchFilters(SearchResults searchResults)
        {
            //if (ApplicationCashManager.filterOptions == null)
            //{
            //    List<FiltersDetail> obj = new List<FiltersDetail>();
            //    var fiters = db.SearchFilters.ToList();
            //    foreach (var item in fiters.OrderBy(d => d.DisplayOrderNo).ToList())
            //    {
            //        var filterdetail = new FiltersDetail
            //        {
            //            FilterName = item.Name,
            //            FilteType = item.FilterType,
            //            FilterControlType = item.FilterControlType.Value,
            //        };

            //        foreach (var filtervalues in item.SearchFilterValues.Where(f => f.Active == true).OrderBy(d => d.DisplayOrder).ToList())
            //        {
            //            //   filterdetail.FilterValues.Add(new System.Collections.Generic.KeyValuePair<string, string>(filtervalues.Name, filtervalues.Value));
            //            if (filtervalues.SearchFilter.FilterType == (int)FilterTypeEnum.Location)
            //            {
            //                filterdetail.FilterValues.Add(new SelectListItem() { Text = filtervalues.Name, Value = filtervalues.Value.ToString() });

            //                if (this.maxLocationValue < Convert.ToInt32(filtervalues.Value.ToString()))
            //                {
            //                    this.maxLocationValue = Convert.ToInt32(filtervalues.Value.ToString());
            //                }
            //            }
            //            else
            //            {
            //                filterdetail.FilterValues.Add(new SelectListItem() { Text = filtervalues.Name, Value = filtervalues.Id.ToString() });
            //            }
            //        }
            //        obj.Add(filterdetail);                    
            //    }

            //    ApplicationCashManager.filterOptions = obj;                
            //}

            searchResults.FilterOptions = ApplicationCashManager.filterOptions;
            this.maxLocationValue = ApplicationCashManager.maxDistanceValue;

        }

        public ActionResult GetResult(FormCollection collection)
        {
            return View("Therapistlist");
        }

        [HttpPost]
        public ActionResult SearchByKeyWords(SearchResults searchResults, string zipCode, string keyworksearch, int pageNo = 1)
        {
            searchResults.CurrentPage = pageNo;
            searchResults.location = zipCode;
            searchResults.keyword = keyworksearch;

            this.FillSearchFilters(searchResults);
            this.SearchTherapist(searchResults, keyworksearch, zipCode, pageNo);
            searchResults.CurrentPage = pageNo;
            this.SetRatting(searchResults);
            this.LogSearchFilters(searchResults, zipCode, keyworksearch);
            return View("Therapistlist", searchResults);
        }

        [HttpPost]
        public JsonResult SendContactEmail(string fullName, string emailAddress, string message, int therapistId)
        {
            string body = "";
            body = body + "Name:" + fullName + "<br>";
            body = body + "Email:" + emailAddress + "<br>";
            body = body + "Message:" + message + "<br>";
            body = body + "Date:" + DateTime.Now.ToShortDateString() + "<br>";
            var theratroveDB = new TheratroveDB();
            var user = theratroveDB.Users.Where(e => e.UserId == therapistId).FirstOrDefault();
            MailAddress mailAddressTo = new MailAddress(user.Email, "Theratrove");
            theratrove.Infrastructure.Helper.SendEmail(mailAddressTo, body, "Contact");
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        public void LogSearchFilters(SearchResults searchResults, string zipCode = "", string Keyword = "", string condition = "")
        {

            SearchFilterLog searchFilterLog = new SearchFilterLog();
            string IP = "";
            string strHostName = "";
            strHostName = System.Net.Dns.GetHostName();

            IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(strHostName);

            IPAddress[] addr = ipEntry.AddressList;

            IP = addr[1].ToString();
            IP = System.Web.HttpContext.Current.Request.UserHostAddress;
            var objIPDetails = theratrove.Infrastructure.Helper.GetIPAddressDetails();
            if (string.IsNullOrEmpty(objIPDetails))
            {
                searchFilterLog.City = "";
                searchFilterLog.State = "";
                searchFilterLog.Country = "";
            }
            else
            {
                var IPDetails = theratrove.Infrastructure.Helper.GetIPAddressDetails().Split(';');
                searchFilterLog.City = IPDetails[6];
                searchFilterLog.State = IPDetails[5];
                searchFilterLog.Country = IPDetails[4];
            }

            string city = searchFilterLog.City;
            string state = searchFilterLog.State;
            string country = searchFilterLog.Country;

            searchFilterLog.Zip = zipCode;
            searchFilterLog.DateCreated = DateTime.Now;
            searchFilterLog.IPAddress = IP;
            searchFilterLog.Keyword = Keyword;
            searchFilterLog.City = city;
            searchFilterLog.State = state;
            searchFilterLog.Country = country;
            searchFilterLog.Condition = condition;

            var itemFaithList = db.SearchFilterValues.Where(e => searchResults.SelectedFaith.Contains(e.Id.ToString())).OrderBy(e=>e.DisplayOrder).ToList();
            foreach (var faititem in itemFaithList)
            {
                searchFilterLog.Faith += faititem.Name + "; ";
            }
            //searchFilterLog.Faith = string.Join("; ", searchResults.SelectedFaith);

            var itemIssuesList = db.SearchFilterValues.Where(e => searchResults.SelectedIssues.Contains(e.Id.ToString())).OrderBy(e => e.DisplayOrder).ToList();
            foreach (var faititem in itemIssuesList)
            {
                searchFilterLog.Issues += faititem.Name + "; ";
            }
            //searchFilterLog.Issues = string.Join("; ", searchResults.SelectedIssues);

            var itemInsuranceList = db.SearchFilterValues.Where(e => searchResults.SelectedInsurance.Contains(e.Id.ToString())).OrderBy(e => e.DisplayOrder).ToList();
            foreach (var faititem in itemInsuranceList)
            {
                searchFilterLog.Insurance += faititem.Name + "; ";
            }
            //searchFilterLog.Insurance = string.Join("; ", searchResults.SelectedInsurance);
            //searchFilterLog.Gender = string.Join("; ", searchResults.SelectedGender);

            var itemLanguageList = db.SearchFilterValues.Where(e => searchResults.SelectedLanguage.Contains(e.Id.ToString())).OrderBy(e => e.DisplayOrder).ToList();
            foreach (var faititem in itemLanguageList)
            {
                searchFilterLog.Language += faititem.Name + "; ";
            }
            //searchFilterLog.Language = string.Join("; ", searchResults.SelectedLanguage);

            var itemLookingForList = db.SearchFilterValues.Where(e => searchResults.SelectedFor.Contains(e.Id.ToString())).OrderBy(e => e.DisplayOrder).ToList();
            foreach (var faititem in itemLookingForList)
            {
                searchFilterLog.LookingFor += faititem.Name + "; ";
            }
            //searchFilterLog.LookingFor = string.Join("; ", searchResults.SelectedFor);

            //var itemLocationList = db.SearchFilterValues.Where(e => searchResults.SelectedLocation.Contains(e.Id.ToString())).ToList();
            //foreach (var faititem in itemLocationList)
            //{
            //    searchFilterLog.Location += faititem.Name + "; ";
            //}
            searchFilterLog.Location = string.Join("miles; ", searchResults.SelectedLocation);

            foreach (var item in searchResults.SelectedGender.Where(d => d != ""))
            {
                if (!string.IsNullOrWhiteSpace(item) && item != "0")
                {
                    var searchFilterValues = db.SearchFilterValues.Where(e => e.Id.ToString() == item).FirstOrDefault();
                    if (searchFilterValues != null)
                    {
                        if (searchResults.SelectedGender.Count == 1)
                            searchFilterLog.Gender = searchFilterValues.Name;
                        else
                            searchFilterLog.Gender = searchFilterLog.Gender + searchFilterValues.Name + "; ";
                    }
                }
            }

            try
            {
                db.SearchFilterLogs.Add(searchFilterLog);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SearchTherapist(SearchResults searchResults, string keyworksearch, string zipCode, int pageNo = 1)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            bool conditionAdded = false;

            conditionAdded = true;
            sb.Append("([Therapist].[Active] = 1) ");

            if (!string.IsNullOrWhiteSpace(keyworksearch))
            {

                conditionAdded = true;
                sb.Append(string.Format(" and ([Therapist].[AboutMe] LIKE '%{0}%') or ([Therapist].[Title] LIKE '%{0}%')", keyworksearch));

            }

            if (searchResults.SelectedGender.Count > 0 && searchResults.SelectedGender.Contains("0") == false && searchResults.SelectedGender[0] != "")
            {

                sb.Append(string.Format(" {0} [GenderPreference].[SearchFilterValueId] in({1})", conditionAdded ? "AND " : "", string.Join(",", searchResults.SelectedGender)));
                conditionAdded = true;
            }

            if (searchResults.SelectedFaith.Count > 0 && searchResults.SelectedFaith[0] != "") //(int)FilterTypeEnum.Faith
            {

                sb.Append(string.Format(" {0} [Faith].[SearchFilterValueId] in({1})", conditionAdded ? "AND " : "", string.Join(",", searchResults.SelectedFaith)));
                conditionAdded = true;
            }

            if (searchResults.SelectedFor.Count > 0 && searchResults.SelectedFor[0] != "") //(int)FilterTypeEnum.For
            {

                sb.Append(string.Format(" {0} [For].[SearchFilterValueId] in({1})", conditionAdded ? "AND " : "", string.Join(",", searchResults.SelectedFor)));
                conditionAdded = true;
            }
            if (searchResults.SelectedIssues.Count > 0 && searchResults.SelectedIssues[0] != "") //(int)FilterTypeEnum.Issues
            {

                sb.Append(string.Format(" {0} [Issues].[SearchFilterValueId] in({1})", conditionAdded ? "AND " : "", string.Join(",", searchResults.SelectedIssues)));
                conditionAdded = true;
            }

            if (searchResults.SelectedInsurance.Count > 0 && searchResults.SelectedInsurance[0] != "") //(int)FilterTypeEnum.Insurance
            {

                sb.Append(string.Format(" {0} [Insurance].[SearchFilterValueId] in({1})", conditionAdded ? "AND " : "", string.Join(",", searchResults.SelectedInsurance)));
                conditionAdded = true;
            }

            if (searchResults.SelectedLanguage.Count > 0 && searchResults.SelectedLanguage[0] != "") //(int)FilterTypeEnum.Insurance
            {

                sb.Append(string.Format(" {0} [Language].[SearchFilterValueId] in({1})", conditionAdded ? "AND " : "", string.Join(",", searchResults.SelectedLanguage)));
                conditionAdded = true;
            }

            List<string> zipcodeList = new List<string>();
            if (!string.IsNullOrWhiteSpace(zipCode))
            {

                int dist = 0;
                if (searchResults.SelectedLocation.Count > 0 && searchResults.SelectedLocation[0] != "")
                {
                    dist = Convert.ToInt32(searchResults.SelectedLocation.First());
                }
                else
                {
                    dist = this.maxLocationValue;
                }
                var zipList = theratrove.Infrastructure.Helper.GetDistance(zipCode, dist);
                if (zipList != null)
                {
                    zipcodeList = zipList.zip_codes.Select(f => f.zip_code).ToList();
                    sb.Append(string.Format(" {0} [Therapist].[ZipCode] in('{1}')", conditionAdded ? "AND " : "", string.Join("','", zipcodeList)));
                }
                else
                {
                    sb.Append(string.Format(" {0} [Therapist].[ZipCode] in({1})", conditionAdded ? "AND " : "", "'-1'"));
                }
                conditionAdded = true;
            }

            sb.Insert(0, " WHERE ");

            List<TherapistSearchResult> output = new List<TherapistSearchResult>();

            SqlParameter parawhere;
            SqlParameter parawherecount;
            if (conditionAdded)
            {
                parawhere = new SqlParameter
                {
                    ParameterName = "wherecondition",
                    Value = sb.ToString()
                };
                parawherecount = new SqlParameter
                {
                    ParameterName = "wherecondition",
                    Value = sb.ToString()
                };
            }
            else
            {
                parawhere = new SqlParameter
                {
                    ParameterName = "wherecondition",
                    Value = DBNull.Value
                };
                parawherecount = new SqlParameter
                {
                    ParameterName = "wherecondition",
                    Value = DBNull.Value
                };
            }

            var parapagesize = new SqlParameter
            {
                ParameterName = "pageno",
                Value = pageNo
            };
            var parapageno = new SqlParameter
            {
                ParameterName = "pagesize",
                Value = this.pageSize
            };
            //Get student name of string type
            output = db.Database.SqlQuery<TherapistSearchResult>("SP_SearchTherapist @wherecondition,@pageno,@pagesize", parawhere, parapageno, parapagesize).ToList<TherapistSearchResult>();
            searchResults.NoOfRecords = db.Database.SqlQuery<int>("SP_TotalRecordsCount @wherecondition", parawherecount).First();

            foreach (var item in output)
            {
                searchResults.TherapistList.Add(new data.Therapist
                {
                    Id = item.Id,
                    FirstName = item.FirstName,
                    LastName = item.LastName,
                    Title = item.Title,
                    AboutMe = item.AboutMe,
                    AcceptInsurance = item.AcceptInsurance,
                    Active = item.Active,
                    Address1 = item.Address1,
                    Address2 = item.Address2,
                    AvgCost = item.AvgCost,
                    City = item.City,
                    Education = item.Education,
                    Gender = item.Gender,
                    License = item.License,
                    Location = item.Location,
                    PhoneNumber = item.PhoneNumber,
                    State = item.State,
                    User = db.Users.Where(us => us.UserId == item.UserId).FirstOrDefault()
                });
            }

            searchResults.CurrentPage = pageNo;

            if (pageNo == 1 && searchResults.NoOfRecords > (pageNo * pageSize))
            {
                searchResults.RecordsRange = string.Format("{0}-{1} of {2}", 1, (pageNo * pageSize), searchResults.NoOfRecords);
            }
            else if ((pageNo * pageSize) >= searchResults.NoOfRecords) //|| (pageNo * pageSize) <= searchResults.NoOfRecords)
            {
                //searchResults.RecordsRange = string.Format("{0} of {1}", ((pageNo - 1 )* (pageSize )), searchResults.NoOfRecords);
                if (pageNo == 1)
                {
                    searchResults.RecordsRange = string.Format("{0} of {1}", searchResults.NoOfRecords, searchResults.NoOfRecords);
                }
                else
                {
                    searchResults.RecordsRange = string.Format("{0} of {1}", ((pageNo - 1) * (pageSize)), searchResults.NoOfRecords);
                }
            }
            else
            {
                searchResults.RecordsRange = string.Format("{0}-{1} of {2}", ((pageNo - 1) * pageSize), (pageNo * pageSize), searchResults.NoOfRecords);
            }
        }

        private void SetRatting(SearchResults searchResults)
        {
            foreach(var therapist in  searchResults.TherapistList)
            {
                RattingList rattingListData = new RattingList();
                rattingListData.Id = therapist.Id;
                rattingListData.Ratting = Math.Round(db.TherapistRatings.Where(e=>e.TherapisttId== therapist.Id).Average(e => e.Rating).GetValueOrDefault(0), 2);
                searchResults.RattingListData.Add(rattingListData);
            }
        }
        [HttpPost]
        public ActionResult AddRaging(ThRating data)
        {
            string IP = "";
            string strHostName = "";
            strHostName = System.Net.Dns.GetHostName();

            IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(strHostName);

            IPAddress[] addr = ipEntry.AddressList;

            IP = addr[1].ToString();
            
            var existingTH = db.TherapistRatings.Where(f => f.TherapisttId == data.thid && f.IPAddress == IP).FirstOrDefault();

            if (existingTH != null)
            {
                existingTH.Rating = data.rating;
                db.SaveChanges();
            }
            else
            {
                db.TherapistRatings.Add(new TherapistRating()
                {
                    IPAddress = IP,
                    Rating = data.rating,
                    TherapisttId = data.thid
                });

                db.SaveChanges();
            }

            var ratingList = db.TherapistRatings.Where(f => f.TherapisttId == data.thid).ToList();
            ViewBag.TherapistRating = Math.Round(ratingList.Average(e => e.Rating).GetValueOrDefault(0), 2);
            ViewBag.TherapistCount = ratingList.Count();
            return Json(new { ratingAverate = ViewBag.TherapistRating , therapistCount=ViewBag.TherapistCount });

        }
    }    
}
