﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web;
using System.Web.Http;
using theratrove.data;
using theratrove.Models;

namespace theratrove.Controllers
{
    public class TheRatroveController : ApiController
    {
        // GET: api/TheRatrove
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/TheRatrove/5
        public IHttpActionResult Get(int id)
        {
            if (id == 0)
            {
                return Content(HttpStatusCode.BadRequest, "invalid user id");
            }

            var theratroveDB = new TheratroveDB();
            User user = theratroveDB.Users.Where(e => e.UserId == id).FirstOrDefault();
            if (user == null)
            {
                return Content(HttpStatusCode.BadRequest, "invalid user id");
            }
            UserModel userModel = GetUserProfile(user, id, theratroveDB);
            return Ok(userModel);
        }
        [HttpGet]
        public IHttpActionResult GetPageContent(string pageName)
        {
            //var theratroveDB = new TheratroveDB();
            //PageContent pageContents = theratroveDB.PageContents.Where(e => e.PageName == pageName).FirstOrDefault();
            //if (pageContents == null)
            //{
            //    return Content(HttpStatusCode.BadRequest, "invalid page name");
            //}
            string pageContents = HttpContext.Current.Request.Url.Host;
            if (HttpContext.Current.Request.IsSecureConnection)
                pageContents = "https://"+ pageContents;
            else
                pageContents = "http://" + pageContents;

            if (ContentType.About.ToString() == pageName)
            {
                pageContents = pageContents + "/home/AboutNoneMenu";
            }
            else if (ContentType.HowItWork.ToString() == pageName)
            {
                pageContents = pageContents + "/home/HowItWorksNoneMenu";
            }
            else if (ContentType.PrivacyPolicy.ToString() == pageName)
            {
                pageContents = pageContents + "/home/PrivacyPolicy";
            }
            else if (ContentType.TermsAndConditions.ToString() == pageName)
            {
                pageContents = pageContents + "/home/TermsAndConditionsNoneMenu";
            }
            
            return Json(new { pageContent = pageContents });
        }


        // POST: api/TheRatrove
        [HttpPost]
        public IHttpActionResult Login(LoginVM loginSignUpVM)
        {
            if (loginSignUpVM == null)
                return Content(HttpStatusCode.BadRequest, "invalid username/password");
            LogingResponseVM logingResponseVM = new LogingResponseVM();
            if (ModelState.IsValid)
            {
                var theratroveDB = new TheratroveDB();
                var userCount = theratroveDB.Users.Where(e => e.UserName == loginSignUpVM.UserName).FirstOrDefault();
                if (userCount == null)
                {
                    return Content(HttpStatusCode.BadRequest, "invalid username/password");
                }
                else
                {
                    if (userCount.Password == loginSignUpVM.Password && userCount.Active == true)
                    {
                        userCount.LastLogin = DateTime.Now;
                        theratroveDB.SaveChanges();

                        logingResponseVM.UserId = userCount.UserId;
                        logingResponseVM.UserName = userCount.UserName;
                        UserModel userModel = GetUserProfile(userCount, userCount.UserId, theratroveDB);
                        return Ok(userModel);
                    }
                    else
                    {
                        userCount.FailedLoginAttempts = userCount.FailedLoginAttempts.GetValueOrDefault(0) + 1;
                        theratroveDB.SaveChanges();
                        return Content(HttpStatusCode.BadRequest, "invalid username/password");
                    }
                }
            }
            else
            {
                string messages = string.Join("; ", ModelState.Values
                                       .SelectMany(x => x.Errors)
                                       .Select(x => x.ErrorMessage));
                return Content(HttpStatusCode.BadRequest, messages);
            }
            // return Ok(logingResponseVM);
        }

        [HttpPost]
        public IHttpActionResult SignUp(SignUpVM signUpVM)
        {
            if (ModelState.IsValid)
            {
                if (signUpVM.Password != signUpVM.ConfirmPassword)
                {
                    ModelState.AddModelError("", "Password and Confirm Password must be same.");
                    return Content(HttpStatusCode.BadRequest, "Password and Confirm Password must be same.");
                }
                var theratroveDB = new TheratroveDB();
                var userCheck = theratroveDB.Users.Where(e => e.UserName == signUpVM.UserName || e.Email == signUpVM.Email).FirstOrDefault();
                if (userCheck == null)
                {
                    User user = new data.User();
                    user.Email = signUpVM.Email;
                    user.UserName = signUpVM.UserName;
                    user.Password = signUpVM.Password;
                    user.CreateDate = DateTime.Now;
                    user.UserType = (byte)UserTypeEnum.Therapist;
                    Guid id = Guid.NewGuid();
                    user.ActivationCode = id.ToString();
                    theratroveDB.Users.Add(user);
                    theratroveDB.SaveChanges();
                    //ViewBag.SucessMessage = "You have been successfully registered. Activation link has been sent to your register email address.";

                    string mailBodyhtml = "Hi " + user.UserName + " and welcome to Theratrove. <br><br>";
                    mailBodyhtml = mailBodyhtml + " TheraTrove is an innovative platform to increase the success of the client/therapist fit. At TheraTrove our mission is to make finding a therapist as easy and efficient as possible. <br><br>";
                    mailBodyhtml = mailBodyhtml + " As a Therapist we are excited to help you get listed on our therapist searching website.  <br><br>";
                    mailBodyhtml = mailBodyhtml + " As a next step please take some time to fill out your profile so that you can be listed and searched by users seeking your type of services.  <br><br>";
                    mailBodyhtml = mailBodyhtml + " If you have any questions or need any assistance please don’t hesitate to contact us.  <br><br>";
                    mailBodyhtml = mailBodyhtml + " Let TheraTrove unlock the treasures of therapy today!  <br><br><br>";
                    mailBodyhtml = mailBodyhtml + " To Active your account please click this link to active your account.  <br>";
                    string url = this.Url.Request.RequestUri.ToString();
                    url = url.Replace(this.Url.Request.RequestUri.LocalPath.ToString(), "");
                    mailBodyhtml = mailBodyhtml + " <a href = '" + string.Format("{0}/Home/Activation?activationcode={1}", url, id.ToString()) + "'>Click here to activate your account.</a><br>";
                    MailAddress mailAddressTo = new MailAddress(user.Email, user.Email);
                    theratrove.Infrastructure.Helper.SendEmail(mailAddressTo, mailBodyhtml, "Welcome Theratrove");
                    return Json(new { userId = user.UserId, userName = user.UserName });
                }
                else
                {
                    if (string.IsNullOrEmpty(userCheck.UserName))
                        return Content(HttpStatusCode.BadRequest, "username already exists.");
                    else if (string.IsNullOrEmpty(userCheck.Email))
                        return Content(HttpStatusCode.BadRequest, "Email already exists.");
                    else
                        return Content(HttpStatusCode.BadRequest, "username or email address already exists");
                }
            }
            else
            {
                string messages = string.Join(Environment.NewLine, ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
                return Content(HttpStatusCode.BadRequest, messages);

            }

        }

        [HttpPost]
        public IHttpActionResult ForgotPassword(ForgotPasswordVM forgotPassword)
        {
            var theratroveDB = new TheratroveDB();
            var user = theratroveDB.Users.Where(e => e.UserName == forgotPassword.UserName || e.Email == forgotPassword.UserName).FirstOrDefault();
            if (user == null)
            {
                return Content(HttpStatusCode.BadRequest, "Username or email does not exists");
            }
            else
            {
                string mailBodyhtml = "Your Theratrove username is " + user.UserName + " password  is: " + user.Password;
                MailAddress mailAddressTo = new MailAddress(user.Email, "Theratrove");
                theratrove.Infrastructure.Helper.SendEmail(mailAddressTo, mailBodyhtml, "Theratrove");
            }
            return Ok("Password sent on your email address");
        }

        [HttpPost]
        public IHttpActionResult Profile(UserWebModel userModel)
        {
            if (userModel.UserId == 0 || userModel.UserId == null)
            {
                return Content(HttpStatusCode.BadRequest, "invalid user id");
            }
            if (ModelState.IsValid)
            {
                TheratroveDB theratroveDB = new TheratroveDB();
                User user = theratroveDB.Users.Where(e => e.UserId == userModel.UserId).FirstOrDefault();
                if (user.Therapist == null)
                    user.Therapist = new Therapist();

                user.Therapist.AboutMe = (userModel.AboutMe == "<br>" ? "" : userModel.AboutMe);
                user.Therapist.AcceptInsurance = userModel.AcceptInsurance;
                user.Therapist.Active = true;
                user.Therapist.Address1 = userModel.Address1;
                user.Therapist.Address2 = userModel.Address2;
                user.Therapist.AvgCost = userModel.AvgCost;
                user.Therapist.City = userModel.City;
                user.Therapist.Education = userModel.Education;
                user.Therapist.FirstName = userModel.FirstName;
                user.Therapist.Gender = userModel.Gender;
                user.Therapist.LastName = userModel.LastName;
                user.Therapist.License = userModel.License;
                user.Therapist.Location = userModel.Location;
                user.Therapist.PhoneNumber = userModel.PhoneNumber;
                user.Therapist.State = userModel.State;
                user.Therapist.Title = userModel.Title;
                user.Therapist.VideoUrl = userModel.VideoUrl;
                user.Therapist.YearsInService = userModel.YearsInService;
                user.Therapist.ZipCode = userModel.ZipCode;
                userModel.ProfilePic = user.ProfilePicURL;
                theratroveDB.TherapistExpertiseFaiths.RemoveRange(user.Therapist.TherapistExpertiseFaiths);
                theratroveDB.TherapistExpertiseFors.RemoveRange(user.Therapist.TherapistExpertiseFors);
                theratroveDB.TherapistExpertiseGenderPreferences.RemoveRange(user.Therapist.TherapistExpertiseGenderPreferences);
                theratroveDB.TherapistExpertiseInsurances.RemoveRange(user.Therapist.TherapistExpertiseInsurances);
                theratroveDB.TherapistExpertiseIssues.RemoveRange(user.Therapist.TherapistExpertiseIssues);
                theratroveDB.TherapistExpertiseLanguages.RemoveRange(user.Therapist.TherapistExpertiseLanguages);
                theratroveDB.TherapistExpertiseLocations.RemoveRange(user.Therapist.TherapistExpertiseLocations);

                var itemList = userModel.SelectedFaith.Where(e => e.Checked == true);
                foreach (var item in itemList)
                {
                    TherapistExpertiseFaith therapistExpertise = new TherapistExpertiseFaith();
                    therapistExpertise.SearchFilterId = item.itemFor;
                    therapistExpertise.SearchFilterValueId = item.Id;
                    user.Therapist.TherapistExpertiseFaiths.Add(therapistExpertise);
                }
                var itemListFor = userModel.SelectedFor.Where(e => e.Checked == true);
                foreach (var item in itemListFor)
                {
                    TherapistExpertiseFor therapistExpertise = new TherapistExpertiseFor();
                    therapistExpertise.SearchFilterId = item.itemFor;
                    therapistExpertise.SearchFilterValueId = item.Id;
                    user.Therapist.TherapistExpertiseFors.Add(therapistExpertise);
                }
                var itemListIssues = userModel.SelectedIssues.Where(e => e.Checked == true);
                foreach (var item in itemListIssues)
                {
                    TherapistExpertiseIssue therapistExpertise = new TherapistExpertiseIssue();
                    therapistExpertise.SearchFilterId = item.itemFor;
                    therapistExpertise.SearchFilterValueId = item.Id;
                    user.Therapist.TherapistExpertiseIssues.Add(therapistExpertise);
                }
                var itemListInsurance = userModel.SelectedInsurance.Where(e => e.Checked == true);
                foreach (var item in itemListInsurance)
                {
                    TherapistExpertiseInsurance therapistExpertise = new TherapistExpertiseInsurance();
                    therapistExpertise.SearchFilterId = item.itemFor;
                    therapistExpertise.SearchFilterValueId = item.Id;
                    user.Therapist.TherapistExpertiseInsurances.Add(therapistExpertise);
                }
                var itemListGenderPreference = userModel.SelectedGenderPreference.Where(e => e.Checked == true);
                foreach (var item in itemListGenderPreference)
                {
                    TherapistExpertiseGenderPreference therapistExpertise = new TherapistExpertiseGenderPreference();
                    therapistExpertise.SearchFilterId = item.itemFor;
                    therapistExpertise.SearchFilterValueId = item.Id;
                    user.Therapist.TherapistExpertiseGenderPreferences.Add(therapistExpertise);
                }

                var itemListLanguage = userModel.SelectedLanguage.Where(e => e.Checked == true);
                foreach (var item in itemListLanguage)
                {
                    TherapistExpertiseLanguage therapistExpertise = new TherapistExpertiseLanguage();
                    therapistExpertise.SearchFilterId = item.itemFor;
                    therapistExpertise.SearchFilterValueId = item.Id;
                    user.Therapist.TherapistExpertiseLanguages.Add(therapistExpertise);
                }

                var itemListLocation = userModel.SelectedLocation.Where(e => e.Checked == true);
                foreach (var item in itemListLocation)
                {
                    TherapistExpertiseLocation therapistExpertise = new TherapistExpertiseLocation();
                    therapistExpertise.SearchFilterId = item.itemFor;
                    therapistExpertise.SearchFilterValueId = item.Id;
                    user.Therapist.TherapistExpertiseLocations.Add(therapistExpertise);
                }

                if (user.Therapist.TherapistTypesOfTherapies.Count > 0)
                {
                    theratroveDB.TherapistTypesOfTherapies.RemoveRange(user.Therapist.TherapistTypesOfTherapies);
                }

                var itemTypesOfTherapyList = userModel.SelectedTherapistTypesOfTherapy.Where(e => e.Checked == true);
                foreach (var item in itemTypesOfTherapyList)
                {
                    TherapistTypesOfTherapy therapistTypesOfTherapy = new TherapistTypesOfTherapy();
                    therapistTypesOfTherapy.TherapyTypesID = item.Id;
                    user.Therapist.TherapistTypesOfTherapies.Add(therapistTypesOfTherapy);
                }
                theratroveDB.SaveChanges();
                userModel.UserId = user.UserId;
            }
            else
            {
                string messages = string.Join("; ", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
                return Content(HttpStatusCode.BadRequest, messages);
            }
            
            return Json(new { profileImageURL = userModel.ProfilePic, userId = userModel.UserId });
        }

        // PUT: api/TheRatrove/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/TheRatrove/5
        public void Delete(int id)
        {
        }

        [HttpPost]
        public IHttpActionResult SendContactEmail(SendContactEmailVM sendContactEmail)
        {
            string body = "";
            body = body + "Name:" + sendContactEmail.FullName + "<br>";
            body = body + "Email:" + sendContactEmail.EmailAddress + "<br>";
            body = body + "Message:" + sendContactEmail.Message + "<br>";
            body = body + "Date:" + DateTime.Now.ToShortDateString() + "<br>";
            var theratroveDB = new TheratroveDB();
            var user = theratroveDB.Users.Where(e => e.UserId == sendContactEmail.TherapistId).FirstOrDefault();
            MailAddress mailAddressTo = new MailAddress(user.Email, "Theratrove");
            theratrove.Infrastructure.Helper.SendEmail(mailAddressTo, body, "Contact");
            return Ok("Email sent successfully.");
        }
        [HttpPost]
        public IHttpActionResult Contact(ContactVM contact)
        {

            var theratroveDB = new TheratroveDB();
            ContactInfo contactInfo = new ContactInfo();
            contactInfo.CreateDate = DateTime.Now;
            contactInfo.Email = contact.Email;
            contactInfo.Message = contact.Message;
            contactInfo.Name = contact.Name;
            contactInfo.Subject = contact.Subject;
            theratroveDB.ContactInfoes.Add(contactInfo);
            theratroveDB.SaveChanges();

            string body = "Thank you for contacting us. One of our support staff will contact you to respond to your question.<br><br>";
            body = body + "Name:" + contact.Name + "<br>";
            body = body + "Email:" + contact.Email + "<br>";
            body = body + "Subject:" + contact.Subject + "<br>";
            body = body + "Message:" + contact.Message + "<br>";
            body = body + "Date:" + DateTime.Now.ToShortDateString() + "<br>";

            var adminUser = theratroveDB.Users.Where(e => e.UserType == (byte)UserTypeEnum.Admin && e.Active == true).ToList();
            foreach (User user in adminUser)
            {
                MailAddress mailAddressTo = new MailAddress(user.Email, "Theratrove");
                theratrove.Infrastructure.Helper.SendEmail(mailAddressTo, body, "Theratrove - Contact Us");
            }
            return Ok("Thank you for contact.");
        }

        [HttpGet]
        public IHttpActionResult GetSearchFilters()
        {
            SearchFilterResponse searchFiltersResponse = new SearchFilterResponse();
            using (TheratroveDB db = new TheratroveDB())
            {
                var fiters = db.SearchFilters.ToList();
                foreach (var item in fiters.OrderBy(d => d.DisplayOrderNo).ToList())
                {
                    foreach (var filtervalues in item.SearchFilterValues.Where(f => f.Active == true).OrderBy(d => d.DisplayOrder).ToList())
                    {                        
                        switch ((FilterTypeEnum)filtervalues.SearchFilter.FilterType)
                        {
                            case FilterTypeEnum.Faith:
                                searchFiltersResponse.FaithOpitions.Add(new KeyValuePair<string, int>(filtervalues.Name, filtervalues.Id));
                                break;
                            case FilterTypeEnum.For:
                                searchFiltersResponse.ForOptions.Add(new KeyValuePair<string, int>(filtervalues.Name, filtervalues.Id));
                                break;
                            case FilterTypeEnum.Issues:
                                searchFiltersResponse.IssuesOptions.Add(new KeyValuePair<string, int>(filtervalues.Name, filtervalues.Id));
                                break;
                            case FilterTypeEnum.Insurance:
                                searchFiltersResponse.InsuranceOptions.Add(new KeyValuePair<string, int>(filtervalues.Name, filtervalues.Id));
                                break;
                            case FilterTypeEnum.GenderPreference:
                                searchFiltersResponse.GenderOptions.Add(new KeyValuePair<string, int>(filtervalues.Name, filtervalues.Id));
                                break;
                            case FilterTypeEnum.Language:
                                searchFiltersResponse.LanguageOptions.Add(new KeyValuePair<string, int>(filtervalues.Name, filtervalues.Id));
                                break;
                            case FilterTypeEnum.Location:
                                searchFiltersResponse.LocationOptions.Add(new KeyValuePair<string, string>(filtervalues.Name, Convert.ToString(filtervalues.Id)));
                                break;
                            default:
                                break;
                        }
                    }
                }

                var typeOfTherapistList = db.TherapyTypes.Where(e => e.Active == true).ToList();
                foreach(TherapyType typeOfTherapist in typeOfTherapistList)
                {
                    searchFiltersResponse.TypeOfTherapist.Add(new KeyValuePair<string, string>(typeOfTherapist.Name, Convert.ToString(typeOfTherapist.Value)));
                }
            }

            return Ok(searchFiltersResponse);
        }

        [HttpPost]
        public IHttpActionResult Search(TherapistSearchVM therapistSearchVM)
        {
            var theratroveDB = new TheratroveDB();
            if (therapistSearchVM == null)
            {
                return Content(HttpStatusCode.BadRequest, "Search parameters are required.");
            }

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            bool conditionAdded = false;

            conditionAdded = true;
            sb.Append("([Therapist].[Active] = 1) ");

            if (therapistSearchVM.PageSize == 0)
            {
                therapistSearchVM.PageSize = int.MaxValue;
            }

            if (therapistSearchVM.PageNo == 0)
            {
                therapistSearchVM.PageNo = 1;
            }
            if (!string.IsNullOrWhiteSpace(therapistSearchVM.Keyword))
            {
                conditionAdded = true;
                sb.Append(string.Format(" and ([Therapist].[AboutMe] LIKE '%{0}%') or ([Therapist].[Title] LIKE '%{0}%')", therapistSearchVM.Keyword));
            }

            if (therapistSearchVM.SelectedGender != null && therapistSearchVM.SelectedGender.Count > 0)
            {
                sb.Append(string.Format(" {0} [GenderPreference].[SearchFilterValueId] in({1})", conditionAdded ? "AND " : "", string.Join(",", therapistSearchVM.SelectedGender)));
                conditionAdded = true;
            }

            if (therapistSearchVM.SelectedFaith != null && therapistSearchVM.SelectedFaith.Count > 0) //(int)FilterTypeEnum.Faith
            {

                sb.Append(string.Format(" {0} [Faith].[SearchFilterValueId] in({1})", conditionAdded ? "AND " : "", string.Join(",", therapistSearchVM.SelectedFaith)));
                conditionAdded = true;
            }

            if (therapistSearchVM.SelectedFor != null && therapistSearchVM.SelectedFor.Count > 0) //(int)FilterTypeEnum.For
            {
                sb.Append(string.Format(" {0} [For].[SearchFilterValueId] in({1})", conditionAdded ? "AND " : "", string.Join(",", therapistSearchVM.SelectedFor)));
                conditionAdded = true;
            }
            if (therapistSearchVM.SelectedIssues != null && therapistSearchVM.SelectedIssues.Count > 0) //(int)FilterTypeEnum.Issues
            {
                sb.Append(string.Format(" {0} [Issues].[SearchFilterValueId] in({1})", conditionAdded ? "AND " : "", string.Join(",", therapistSearchVM.SelectedIssues)));
                conditionAdded = true;
            }

            if (therapistSearchVM.SelectedInsurance != null && therapistSearchVM.SelectedInsurance.Count > 0) //(int)FilterTypeEnum.Insurance
            {
                sb.Append(string.Format(" {0} [Insurance].[SearchFilterValueId] in({1})", conditionAdded ? "AND " : "", string.Join(",", therapistSearchVM.SelectedInsurance)));
                conditionAdded = true;
            }

            if (therapistSearchVM.SelectedLanguage != null && therapistSearchVM.SelectedLanguage.Count > 0) //(int)FilterTypeEnum.Insurance
            {
                sb.Append(string.Format(" {0} [Language].[SearchFilterValueId] in({1})", conditionAdded ? "AND " : "", string.Join(",", therapistSearchVM.SelectedLanguage)));
                conditionAdded = true;
            }

            List<string> zipcodeList = new List<string>();
            if (!string.IsNullOrWhiteSpace(therapistSearchVM.ZipCode))
            {
                if (therapistSearchVM.MaxDistance == 0)
                {
                    var maxdist = theratroveDB.SearchFilterValues.Where(f => f.SearchFilter.FilterType == (int)FilterTypeEnum.Location).Max(d => d.Value);
                    therapistSearchVM.MaxDistance = Convert.ToInt32(maxdist);
                }

                var zipList = theratrove.Infrastructure.Helper.GetDistance(therapistSearchVM.ZipCode, therapistSearchVM.MaxDistance);
                if (zipList != null)
                {
                    zipcodeList = zipList.zip_codes.Select(f => f.zip_code).ToList();
                    sb.Append(string.Format(" {0} [Therapist].[ZipCode] in('{1}')", conditionAdded ? "AND " : "", string.Join("','", zipcodeList)));
                }
                else
                {
                    sb.Append(string.Format(" {0} [Therapist].[ZipCode] in({1})", conditionAdded ? "AND " : "", "'-1'"));
                }
                conditionAdded = true;
            }

            sb.Insert(0, " WHERE ");

            List<TherapistSearchResult> output = new List<TherapistSearchResult>();

            SqlParameter parawhere;
            SqlParameter parawherecount;
            if (conditionAdded)
            {
                parawhere = new SqlParameter
                {
                    ParameterName = "wherecondition",
                    Value = sb.ToString()
                };
                parawherecount = new SqlParameter
                {
                    ParameterName = "wherecondition",
                    Value = sb.ToString()
                };
            }
            else
            {
                parawhere = new SqlParameter
                {
                    ParameterName = "wherecondition",
                    Value = DBNull.Value
                };
                parawherecount = new SqlParameter
                {
                    ParameterName = "wherecondition",
                    Value = DBNull.Value
                };
            }

            var parapagesize = new SqlParameter
            {
                ParameterName = "pageno",
                Value = therapistSearchVM.PageNo
            };
            var parapageno = new SqlParameter
            {
                ParameterName = "pagesize",
                Value = therapistSearchVM.PageSize
            };
            //Get student name of string type
            output = theratroveDB.Database.SqlQuery<TherapistSearchResult>("SP_SearchTherapist @wherecondition,@pageno,@pagesize", parawhere, parapageno, parapagesize).ToList<TherapistSearchResult>();
            int NoOfRecords = theratroveDB.Database.SqlQuery<int>("SP_TotalRecordsCount @wherecondition", parawherecount).First();

            // List<UserModel> result = new List<UserModel>();
            List<TherapistResult> result = new List<TherapistResult>();
            foreach (var item in output)
            {
                //var userModel = new UserModel();
                var userdata = theratroveDB.Users.Where(f => f.UserId == item.UserId).FirstOrDefault();

                result.Add(new TherapistResult
                {
                    //UserId = item.Id,
                    Id = item.Id,
                    FirstName = item.FirstName,
                    LastName = item.LastName,
                    Title = item.Title,
                    AboutMe = item.AboutMe,
                    AcceptInsurance = item.AcceptInsurance,
                    Address1 = item.Address1,
                    Address2 = item.Address2,
                    AvgCost = item.AvgCost,
                    City = item.City,
                    Education = item.Education,
                    Gender = item.Gender == 1 ? "Male" : "Female",
                    License = item.License,
                    Location = item.Location,
                    PhoneNumber = item.PhoneNumber,
                    State = item.State,
                    TherapistRating = Math.Round(theratroveDB.TherapistRatings.Where(e => e.TherapisttId == item.UserId).Average(e => e.Rating).GetValueOrDefault(0), 2),
                    Email = userdata.Email,
                    ProfilePicURL = userdata.ProfilePicURL,
                    //  TherapyGalleryList = userdata.TherapyGalleries.ToList(),                  
                    VideoUrl = (string.IsNullOrEmpty(userdata.Therapist.VideoUrl) ? "-" : userdata.Therapist.VideoUrl),
                    YearsInService = item.YearsInService,
                    LikeCount=item.LikeCount,
                    ZipCode = item.ZipCode
                    //FaithFilter = string.Join(", ", userdata.Therapist.TherapistExpertiseFaiths.Where(e => e.SearchFilter.FilterType == (byte)FilterTypeEnum.Faith).Select(e => e.SearchFilterValue.Name)),
                    //ForFilter = string.Join(", ", userdata.Therapist.TherapistExpertiseFors.Where(e => e.SearchFilter.FilterType == (byte)FilterTypeEnum.For).Select(e => e.SearchFilterValue.Name)),
                    //IssuesFilter = string.Join(", ", userdata.Therapist.TherapistExpertiseIssues.Where(e => e.SearchFilter.FilterType == (byte)FilterTypeEnum.Issues).Select(e => e.SearchFilterValue.Name)),
                    //InsuranceZipCodeFilter = string.Join(", ", userdata.Therapist.TherapistExpertiseInsurances.Where(e => e.SearchFilter.FilterType == (byte)FilterTypeEnum.Insurance).Select(e => e.SearchFilterValue.Name)),
                    //GenderPreferenceFilter = string.Join(", ", userdata.Therapist.TherapistExpertiseGenderPreferences.Where(e => e.SearchFilter.FilterType == (byte)FilterTypeEnum.GenderPreference).Select(e => e.SearchFilterValue.Name)),
                    //LanguageFilter = string.Join(", ", userdata.Therapist.TherapistExpertiseLanguages.Where(e => e.SearchFilter.FilterType == (byte)FilterTypeEnum.Language).Select(e => e.SearchFilterValue.Name)),
                    //LocationFilter = string.Join(", ", userdata.Therapist.TherapistExpertiseLocations.Where(e => e.SearchFilter.FilterType == (byte)FilterTypeEnum.Location).Select(e => e.SearchFilterValue.Name))
                });
            }
            
            return Ok(new { Result = result });
        }
        [HttpPost]
        public IHttpActionResult SearchByRating(TherapistSearchVM therapistSearchVM)
        {
            var theratroveDB = new TheratroveDB();
            if (therapistSearchVM == null)
            {
                return Content(HttpStatusCode.BadRequest, "Search parameters are required.");
            }

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            bool conditionAdded = false;

            conditionAdded = true;
            sb.Append("([Therapist].[Active] = 1) ");

            if (therapistSearchVM.PageSize == 0)
            {
                therapistSearchVM.PageSize = int.MaxValue;
            }

            if (therapistSearchVM.PageNo == 0)
            {
                therapistSearchVM.PageNo = 1;
            }
            if (!string.IsNullOrWhiteSpace(therapistSearchVM.Keyword))
            {
                conditionAdded = true;
                sb.Append(string.Format(" and ([Therapist].[AboutMe] LIKE '%{0}%') or ([Therapist].[Title] LIKE '%{0}%')", therapistSearchVM.Keyword));
            }

            if (therapistSearchVM.SelectedGender != null && therapistSearchVM.SelectedGender.Count > 0)
            {
                sb.Append(string.Format(" {0} [GenderPreference].[SearchFilterValueId] in({1})", conditionAdded ? "AND " : "", string.Join(",", therapistSearchVM.SelectedGender)));
                conditionAdded = true;
            }

            if (therapistSearchVM.SelectedFaith != null && therapistSearchVM.SelectedFaith.Count > 0) //(int)FilterTypeEnum.Faith
            {

                sb.Append(string.Format(" {0} [Faith].[SearchFilterValueId] in({1})", conditionAdded ? "AND " : "", string.Join(",", therapistSearchVM.SelectedFaith)));
                conditionAdded = true;
            }

            if (therapistSearchVM.SelectedFor != null && therapistSearchVM.SelectedFor.Count > 0) //(int)FilterTypeEnum.For
            {
                sb.Append(string.Format(" {0} [For].[SearchFilterValueId] in({1})", conditionAdded ? "AND " : "", string.Join(",", therapistSearchVM.SelectedFor)));
                conditionAdded = true;
            }
            if (therapistSearchVM.SelectedIssues != null && therapistSearchVM.SelectedIssues.Count > 0) //(int)FilterTypeEnum.Issues
            {
                sb.Append(string.Format(" {0} [Issues].[SearchFilterValueId] in({1})", conditionAdded ? "AND " : "", string.Join(",", therapistSearchVM.SelectedIssues)));
                conditionAdded = true;
            }

            if (therapistSearchVM.SelectedInsurance != null && therapistSearchVM.SelectedInsurance.Count > 0) //(int)FilterTypeEnum.Insurance
            {
                sb.Append(string.Format(" {0} [Insurance].[SearchFilterValueId] in({1})", conditionAdded ? "AND " : "", string.Join(",", therapistSearchVM.SelectedInsurance)));
                conditionAdded = true;
            }

            if (therapistSearchVM.SelectedLanguage != null && therapistSearchVM.SelectedLanguage.Count > 0) //(int)FilterTypeEnum.Insurance
            {
                sb.Append(string.Format(" {0} [Language].[SearchFilterValueId] in({1})", conditionAdded ? "AND " : "", string.Join(",", therapistSearchVM.SelectedLanguage)));
                conditionAdded = true;
            }

            List<string> zipcodeList = new List<string>();
            if (!string.IsNullOrWhiteSpace(therapistSearchVM.ZipCode))
            {
                if (therapistSearchVM.MaxDistance == 0)
                {
                    var maxdist = theratroveDB.SearchFilterValues.Where(f => f.SearchFilter.FilterType == (int)FilterTypeEnum.Location).Max(d => d.Value);
                    therapistSearchVM.MaxDistance = Convert.ToInt32(maxdist);
                }

                var zipList = theratrove.Infrastructure.Helper.GetDistance(therapistSearchVM.ZipCode, therapistSearchVM.MaxDistance);
                if (zipList != null)
                {
                    zipcodeList = zipList.zip_codes.Select(f => f.zip_code).ToList();
                    sb.Append(string.Format(" {0} [Therapist].[ZipCode] in('{1}')", conditionAdded ? "AND " : "", string.Join("','", zipcodeList)));
                }
                else
                {
                    sb.Append(string.Format(" {0} [Therapist].[ZipCode] in({1})", conditionAdded ? "AND " : "", "'-1'"));
                }
                conditionAdded = true;
            }

            sb.Insert(0, " WHERE ");

            List<TherapistSearchResult> output = new List<TherapistSearchResult>();

            SqlParameter parawhere;
            SqlParameter parawherecount;
            if (conditionAdded)
            {
                parawhere = new SqlParameter
                {
                    ParameterName = "wherecondition",
                    Value = sb.ToString()
                };
                parawherecount = new SqlParameter
                {
                    ParameterName = "wherecondition",
                    Value = sb.ToString()
                };
            }
            else
            {
                parawhere = new SqlParameter
                {
                    ParameterName = "wherecondition",
                    Value = DBNull.Value
                };
                parawherecount = new SqlParameter
                {
                    ParameterName = "wherecondition",
                    Value = DBNull.Value
                };
            }

            var parapagesize = new SqlParameter
            {
                ParameterName = "pageno",
                Value = therapistSearchVM.PageNo
            };
            var parapageno = new SqlParameter
            {
                ParameterName = "pagesize",
                Value = therapistSearchVM.PageSize
            };
            //Get student name of string type
            output = theratroveDB.Database.SqlQuery<TherapistSearchResult>("SP_SearchTherapistByRating @wherecondition,@pageno,@pagesize", parawhere, parapageno, parapagesize).ToList<TherapistSearchResult>();
            int NoOfRecords = theratroveDB.Database.SqlQuery<int>("SP_TotalRecordsCount @wherecondition", parawherecount).First();

            // List<UserModel> result = new List<UserModel>();
            List<TherapistResult> result = new List<TherapistResult>();
            foreach (var item in output)
            {
                //var userModel = new UserModel();
                var userdata = theratroveDB.Users.Where(f => f.UserId == item.UserId).FirstOrDefault();

                result.Add(new TherapistResult
                {
                    //UserId = item.Id,
                    Id = item.Id,
                    FirstName = item.FirstName,
                    LastName = item.LastName,
                    Title = item.Title,
                    AboutMe = item.AboutMe,
                    AcceptInsurance = item.AcceptInsurance,
                    Address1 = item.Address1,
                    Address2 = item.Address2,
                    AvgCost = item.AvgCost,
                    City = item.City,
                    Education = item.Education,
                    Gender = item.Gender == 1 ? "Male" : "Female",
                    License = item.License,
                    Location = item.Location,
                    PhoneNumber = item.PhoneNumber,
                    State = item.State,
                    TherapistRating = Math.Round(item.TherapistRatingAvg.GetValueOrDefault(0), 2),
                    Email = userdata.Email,
                    ProfilePicURL = userdata.ProfilePicURL,
                    //       ProfilePic = userdata.ProfilePic,
                    //  TherapyGalleryList = userdata.TherapyGalleries.ToList(),                  
                    VideoUrl = (string.IsNullOrEmpty(userdata.Therapist.VideoUrl) ? "-" : userdata.Therapist.VideoUrl),
                    YearsInService = item.YearsInService,
                    ZipCode = item.ZipCode,
                    LikeCount= item.LikeCount
                    //FaithFilter = string.Join(", ", userdata.Therapist.TherapistExpertiseFaiths.Where(e => e.SearchFilter.FilterType == (byte)FilterTypeEnum.Faith).Select(e => e.SearchFilterValue.Name)),
                    //ForFilter = string.Join(", ", userdata.Therapist.TherapistExpertiseFors.Where(e => e.SearchFilter.FilterType == (byte)FilterTypeEnum.For).Select(e => e.SearchFilterValue.Name)),
                    //IssuesFilter = string.Join(", ", userdata.Therapist.TherapistExpertiseIssues.Where(e => e.SearchFilter.FilterType == (byte)FilterTypeEnum.Issues).Select(e => e.SearchFilterValue.Name)),
                    //InsuranceZipCodeFilter = string.Join(", ", userdata.Therapist.TherapistExpertiseInsurances.Where(e => e.SearchFilter.FilterType == (byte)FilterTypeEnum.Insurance).Select(e => e.SearchFilterValue.Name)),
                    //GenderPreferenceFilter = string.Join(", ", userdata.Therapist.TherapistExpertiseGenderPreferences.Where(e => e.SearchFilter.FilterType == (byte)FilterTypeEnum.GenderPreference).Select(e => e.SearchFilterValue.Name)),
                    //LanguageFilter = string.Join(", ", userdata.Therapist.TherapistExpertiseLanguages.Where(e => e.SearchFilter.FilterType == (byte)FilterTypeEnum.Language).Select(e => e.SearchFilterValue.Name)),
                    //LocationFilter = string.Join(", ", userdata.Therapist.TherapistExpertiseLocations.Where(e => e.SearchFilter.FilterType == (byte)FilterTypeEnum.Location).Select(e => e.SearchFilterValue.Name))
                });
            }

            return Ok(new { Result = result });
        }

        [HttpPost]
        public IHttpActionResult ProfilePicUpload(PictureVM pictureVM)
        {
            var theratroveDB = new TheratroveDB();
            int userId = pictureVM.TherapistId;
            User user = theratroveDB.Users.Where(e => e.UserId == userId).FirstOrDefault();
            if(!string.IsNullOrEmpty(user.ProfilePicURL))
            {
                theratrove.Infrastructure.Helper.DeleteImage(user.ProfilePicURL, "profile");
            }
            string urlImage = theratrove.Infrastructure.Helper.SaveImageByteArray(pictureVM.PictureFile, "profile", pictureVM.PictureFileName);
            user.ProfilePicURL = urlImage;
            theratroveDB.SaveChanges();

            return Json(new { profileImageURL = user.ProfilePicURL, userId = user.UserId });
        }

        [HttpPost]
        public IHttpActionResult GalleryPicUpload(PictureVM pictureVM)
        {
            var theratroveDB = new TheratroveDB();
            int userId = pictureVM.TherapistId;
            User user = theratroveDB.Users.Where(e => e.UserId == userId).FirstOrDefault();
            TherapyGallery therapyGallery = new TherapyGallery();
            string urlImage = theratrove.Infrastructure.Helper.SaveImageByteArray(pictureVM.PictureFile, "therapygallery", pictureVM.PictureFileName);
            therapyGallery.GalleryImageURL = urlImage;
            therapyGallery.CreateDateTime = DateTime.Now;
            user.TherapyGalleries.Add(therapyGallery);
            theratroveDB.SaveChanges();
            return Json(new { urlImage = urlImage, therapyGalleryId = therapyGallery.Id });
        }

        [HttpPost]
        public IHttpActionResult DeleteGalleryPic(PictureVM pictureVM)
        {
            var theratroveDB = new TheratroveDB();
            int userId = pictureVM.TherapistId;
            TherapyGallery therapyGallery = theratroveDB.TherapyGalleries.Where(e => e.Id == pictureVM.PictureId && e.UserId == userId).FirstOrDefault();
            if (therapyGallery != null)
            {
                if (!string.IsNullOrEmpty(therapyGallery.GalleryImageURL))
                {
                    theratrove.Infrastructure.Helper.DeleteImage(therapyGallery.GalleryImageURL, "therapygallery");
                }
                theratroveDB.TherapyGalleries.Remove(therapyGallery);
                theratroveDB.SaveChanges();
            }
            return Ok("Successfully deleted");
        }
        private UserModel GetUserProfile(User user, int id, TheratroveDB theratroveDB)
        {
            UserModel userModel = new UserModel();
            userModel.UserId = id;
            userModel.Email = user.Email;
            userModel.UserName = user.UserName;
            userModel.Active = user.Active;
            userModel.ProfilePic = user.ProfilePicURL;
            userModel.TherapyGalleryList = user.TherapyGalleries.ToList();
            userModel.LikeCount = theratroveDB.TherapistLikes.Where(e => e.TherapistId == id).Count();
            var SearchFilter = theratroveDB.SearchFilterValues.Where(e => e.Active == true).ToList();
            foreach (var item in SearchFilter)
            {
                CheckboxModel checkboxModel = new CheckboxModel();
                checkboxModel.Id = item.Id;
                checkboxModel.Name = item.Name;
                checkboxModel.itemFor = item.SearchFilterID;
                if (item.SearchFilter.FilterType == (byte)FilterTypeEnum.Faith)
                    userModel.SelectedFaith.Add(checkboxModel);
                if (item.SearchFilter.FilterType == (byte)FilterTypeEnum.For)
                    userModel.SelectedFor.Add(checkboxModel);
                if (item.SearchFilter.FilterType == (byte)FilterTypeEnum.GenderPreference)
                    userModel.SelectedGenderPreference.Add(checkboxModel);
                if (item.SearchFilter.FilterType == (byte)FilterTypeEnum.Insurance)
                    userModel.SelectedInsurance.Add(checkboxModel);
                if (item.SearchFilter.FilterType == (byte)FilterTypeEnum.Issues)
                    userModel.SelectedIssues.Add(checkboxModel);
                if (item.SearchFilter.FilterType == (byte)FilterTypeEnum.Language)
                    userModel.SelectedLanguage.Add(checkboxModel);
                if (item.SearchFilter.FilterType == (byte)FilterTypeEnum.Location)
                    userModel.SelectedLocation.Add(checkboxModel);
            }
            var typesOfTherapy = theratroveDB.TherapyTypes.Where(e => e.Active == true).ToList();
            foreach (var item in typesOfTherapy)
            {
                CheckboxModel checkboxModel = new CheckboxModel();
                checkboxModel.Id = item.Id;
                checkboxModel.Name = item.Name;
                userModel.SelectedTherapistTypesOfTherapy.Add(checkboxModel);
            }
            userModel.UserId = user.UserId;

            if (user.Therapist != null)
            {

                var ratingList = user.Therapist.TherapistRatings.ToList();
                userModel.TherapistRating = Math.Round(ratingList.Average(e => e.Rating).GetValueOrDefault(0), 2);
                userModel.TherapistRatingCount = ratingList.Count();
                
                userModel.AboutMe = (string.IsNullOrEmpty(user.Therapist.AboutMe) ? "" : user.Therapist.AboutMe);
                userModel.AcceptInsurance = user.Therapist.AcceptInsurance;
                userModel.Address1 = (string.IsNullOrEmpty(user.Therapist.Address1) ? "" : user.Therapist.Address1);
                userModel.Address2 = (string.IsNullOrEmpty(user.Therapist.Address2) ? "" : user.Therapist.Address2);
                userModel.AvgCost = user.Therapist.AvgCost;
                userModel.City = (string.IsNullOrEmpty(user.Therapist.City) ? "" : user.Therapist.City);
                userModel.Education = (string.IsNullOrEmpty(user.Therapist.Education) ? "" : user.Therapist.Education);
                userModel.FirstName = (string.IsNullOrEmpty(user.Therapist.FirstName) ? "" : user.Therapist.FirstName);
                userModel.Gender = user.Therapist.Gender;
                userModel.LastName = (string.IsNullOrEmpty(user.Therapist.LastName) ? "" : user.Therapist.LastName);
                userModel.License = (string.IsNullOrEmpty(user.Therapist.License) ? "" : user.Therapist.License);
                userModel.Location = (string.IsNullOrEmpty(user.Therapist.Location) ? "" : user.Therapist.Location);
                userModel.PhoneNumber = (string.IsNullOrEmpty(user.Therapist.PhoneNumber) ? "" : user.Therapist.PhoneNumber);
                userModel.State = (string.IsNullOrEmpty(user.Therapist.State) ? "" : user.Therapist.State);
                userModel.Title = (string.IsNullOrEmpty(user.Therapist.Title) ? "" : user.Therapist.Title);
                userModel.VideoUrl = (string.IsNullOrEmpty(user.Therapist.VideoUrl) ? "" : user.Therapist.VideoUrl);
                userModel.YearsInService = (string.IsNullOrEmpty(user.Therapist.YearsInService) ? "" : user.Therapist.YearsInService);
                userModel.ZipCode = (string.IsNullOrEmpty(user.Therapist.ZipCode) ? "" : user.Therapist.ZipCode);
                userModel.FaithFilter = string.Join(", ", user.Therapist.TherapistExpertiseFaiths.Where(e => e.SearchFilter.FilterType == (byte)FilterTypeEnum.Faith).Select(e => e.SearchFilterValue.Name));
                userModel.ForFilter = string.Join(", ", user.Therapist.TherapistExpertiseFors.Where(e => e.SearchFilter.FilterType == (byte)FilterTypeEnum.For).Select(e => e.SearchFilterValue.Name));
                userModel.IssuesFilter = string.Join(", ", user.Therapist.TherapistExpertiseIssues.Where(e => e.SearchFilter.FilterType == (byte)FilterTypeEnum.Issues).Select(e => e.SearchFilterValue.Name));
                userModel.InsuranceZipCodeFilter = string.Join(", ", user.Therapist.TherapistExpertiseInsurances.Where(e => e.SearchFilter.FilterType == (byte)FilterTypeEnum.Insurance).Select(e => e.SearchFilterValue.Name));
                userModel.GenderPreferenceFilter = string.Join(", ", user.Therapist.TherapistExpertiseGenderPreferences.Where(e => e.SearchFilter.FilterType == (byte)FilterTypeEnum.GenderPreference).Select(e => e.SearchFilterValue.Name));
                userModel.LanguageFilter = string.Join(", ", user.Therapist.TherapistExpertiseLanguages.Where(e => e.SearchFilter.FilterType == (byte)FilterTypeEnum.Language).Select(e => e.SearchFilterValue.Name));
                userModel.LocationFilter = string.Join(", ", user.Therapist.TherapistExpertiseLocations.Where(e => e.SearchFilter.FilterType == (byte)FilterTypeEnum.Location).Select(e => e.SearchFilterValue.Name));
                userModel.TherapistTypesOfTherapyFilter = string.Join(", ", user.Therapist.TherapistTypesOfTherapies.Select(e => e.TherapyType.Name));

                var therapistTypesOfTherapiesList = user.Therapist.TherapistTypesOfTherapies.ToList();
                foreach (var therapistTypesOfTherapie in therapistTypesOfTherapiesList)
                {
                    var item = userModel.SelectedTherapistTypesOfTherapy.Where(e => e.Id == therapistTypesOfTherapie.TherapyTypesID).FirstOrDefault();
                    if (item != null)
                    {
                        item.Checked = true;
                    }
                }


                var therapistExpertisesListFaiths = user.Therapist.TherapistExpertiseFaiths.ToList();
                foreach (var therapistExpertiseFaiths in therapistExpertisesListFaiths)
                {
                    var item = userModel.SelectedFaith.Where(e => e.Id == therapistExpertiseFaiths.SearchFilterValueId).FirstOrDefault();
                    if (item != null)
                    {
                        item.Checked = true;
                    }
                }

                var therapistExpertisesListFor = user.Therapist.TherapistExpertiseFors.ToList();
                foreach (var therapistExpertiseFors in therapistExpertisesListFor)
                {
                    var item = userModel.SelectedFor.Where(e => e.Id == therapistExpertiseFors.SearchFilterValueId).FirstOrDefault();
                    if (item != null)
                    {
                        item.Checked = true;
                    }
                }


                var therapistExpertisesListIssues = user.Therapist.TherapistExpertiseIssues.ToList();
                foreach (var therapistExpertiseIssues in therapistExpertisesListIssues)
                {
                    var item = userModel.SelectedIssues.Where(e => e.Id == therapistExpertiseIssues.SearchFilterValueId).FirstOrDefault();
                    if (item != null)
                    {
                        item.Checked = true;
                    }
                }

                var therapistExpertisesListInsurance = user.Therapist.TherapistExpertiseInsurances.ToList();
                foreach (var therapistExpertiseInsurance in therapistExpertisesListInsurance)
                {
                    var item = userModel.SelectedInsurance.Where(e => e.Id == therapistExpertiseInsurance.SearchFilterValueId).FirstOrDefault();
                    if (item != null)
                    {
                        item.Checked = true;
                    }
                }

                var therapistExpertisesListGenderPreference = user.Therapist.TherapistExpertiseGenderPreferences.ToList();
                foreach (var therapistExpertiseGenderPreference in therapistExpertisesListGenderPreference)
                {
                    var item = userModel.SelectedGenderPreference.Where(e => e.Id == therapistExpertiseGenderPreference.SearchFilterValueId).FirstOrDefault();
                    if (item != null)
                    {
                        item.Checked = true;
                    }
                }

                var therapistExpertisesListLanguage = user.Therapist.TherapistExpertiseLanguages.ToList();
                foreach (var therapistExpertiseLanguage in therapistExpertisesListLanguage)
                {
                    var item = userModel.SelectedLanguage.Where(e => e.Id == therapistExpertiseLanguage.SearchFilterValueId).FirstOrDefault();
                    if (item != null)
                    {
                        item.Checked = true;
                    }
                }

                var therapistExpertisesListLocation = user.Therapist.TherapistExpertiseLocations.ToList();
                foreach (var therapistExpertiseLocation in therapistExpertisesListLocation)
                {
                    var item = userModel.SelectedLocation.Where(e => e.Id == therapistExpertiseLocation.SearchFilterValueId).FirstOrDefault();
                    if (item != null)
                    {
                        item.Checked = true;
                    }
                }

            }
            return userModel;
        }

        [HttpPost]
        public IHttpActionResult ChangePassword(ChangePasswordVM userModel)
        {

            int userId = userModel.UserId.GetValueOrDefault(0);
            if (userId == 0)
            {
                return Content(HttpStatusCode.BadRequest, "");
            }

            if (ModelState.IsValid)
            {
                if (userModel.Password != userModel.ConfirmPassword)
                {
                    return Content(HttpStatusCode.BadRequest, "Password and Confirm Password must be same.");
                }
                var theratroveDB = new TheratroveDB();
                var userCount = theratroveDB.Users.Where(e => e.UserId == userId).FirstOrDefault();
                if (userCount != null)
                {
                    userCount.Password = userModel.Password;
                    theratroveDB.SaveChanges();
                }
                else
                {
                    return Content(HttpStatusCode.BadRequest, "Your password is not changes!");
                }
            }
            else
            {
                string messages = string.Join("; ", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
                return Content(HttpStatusCode.BadRequest, messages);
            }
            return Ok("Your password has been successfully changed.");
        }

        [HttpPost]
        public IHttpActionResult AddRaging(ThRating data)
        {
            string IP = "";
            string strHostName = "";
            strHostName = System.Net.Dns.GetHostName();

            IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(strHostName);
            IPAddress[] addr = ipEntry.AddressList;

            IP = addr[1].ToString();
            var db = new TheratroveDB();
            var existingTH = db.TherapistRatings.Where(f => f.TherapisttId == data.TherapisttId && f.IPAddress == IP).FirstOrDefault();

            if (existingTH != null)
            {
                existingTH.Rating = data.rating;
                db.SaveChanges();
            }
            else
            {
                db.TherapistRatings.Add(new TherapistRating()
                {
                    IPAddress = IP,
                    Rating = data.rating,
                    TherapisttId = data.thid
                });

                db.SaveChanges();
            }

            var ratingList = db.TherapistRatings.Where(f => f.TherapisttId == data.thid).ToList();
            var TherapistRating = Math.Round(ratingList.Average(e => e.Rating).GetValueOrDefault(0), 2);
            var TherapistCount = ratingList.Count();
            return Json(new { ratingAverate = TherapistRating, therapistCount = TherapistCount });
        }

        [HttpPost]
        public IHttpActionResult AddLike(TherapistLikeVM data)
        {
            var db = new TheratroveDB();
            var therapist = db.TherapistLikes.Where(e => e.TherapistId == data.TherapisttId && e.UserId ==data.UserId).FirstOrDefault();
            
            if (therapist == null)
            {
                therapist = new TherapistLike();
                therapist.UserId = data.UserId;
                therapist.TherapistId = data.TherapisttId;
                db.TherapistLikes.Add(therapist);
                db.SaveChanges();
            }
            int likeCount = db.TherapistLikes.Where(e=>e.TherapistId== data.TherapisttId).Count();
            return Json(new { likeCount = likeCount });
        }


    }
}
