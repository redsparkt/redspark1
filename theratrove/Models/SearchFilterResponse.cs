﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace theratrove.Models
{
    public class SearchFilterResponse
    {
        public SearchFilterResponse()
        {
            this.ForOptions = new List<KeyValuePair<string, int>>();
            this.FaithOpitions = new List<KeyValuePair<string, int>>();
            this.GenderOptions = new List<KeyValuePair<string, int>>();
            this.IssuesOptions = new List<KeyValuePair<string, int>>();
            this.InsuranceOptions = new List<KeyValuePair<string, int>>();
            this.LanguageOptions=new List<KeyValuePair<string, int>>();
            this.LocationOptions = new List<KeyValuePair<string, string>>();
            this.TypeOfTherapist = new List<KeyValuePair<string, string>>();
        }
        public List<KeyValuePair<string, int>> ForOptions { get; set; }
        public List<KeyValuePair<string, int>> FaithOpitions { get; set; }
        public List<KeyValuePair<string, int>> GenderOptions { get; set; }
        public List<KeyValuePair<string, int>> IssuesOptions { get; set; }
        public List<KeyValuePair<string, int>> InsuranceOptions { get; set; }
        public List<KeyValuePair<string, int>> LanguageOptions { get; set; }

        public List<KeyValuePair<string, string>> LocationOptions { get; set; }
        public List<KeyValuePair<string, string>> TypeOfTherapist { get; set; }

    }
}