﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace theratrove.Models
{
    public enum UserTypeEnum
    {
        Therapist = 1,
        Admin = 2
    }
    public enum ContentType
    {
        PrivacyPolicy = 1,
        TermsAndConditions = 2,
        About = 3,
        HowItWork=4
    }
}