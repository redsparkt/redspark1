﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace theratrove.Models
{


    public class IndexPageCounts
    {
        public int TotalSearch { get; set; }

        public int NoofTherapist { get; set; }

        public int Members { get; set; }
    }
}