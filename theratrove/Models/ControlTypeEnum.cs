﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace theratrove.Models
{
    public enum ControlTypeEnum
    {
        CheckBox = 1,
        DropDown = 2
    }
}
public class ControlType
{
    public int Id { get; set; }
    public string ControlName { get; set; }
}