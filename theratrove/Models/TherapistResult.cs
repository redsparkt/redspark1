﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace theratrove.Models
{
    public class TherapistResult
    {
        public int Id { get; set; }

        public string Email { get; set; }
        public string ProfilePicURL { get; set; }
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Title { get; set; }

        public string YearsInService { get; set; }

        public string Education { get; set; }

        public bool AcceptInsurance { get; set; }

        public string License { get; set; }

        public string AboutMe { get; set; }

        public string VideoUrl { get; set; }
        public string PhoneNumber { get; set; }

        public string Location { get; set; }

        public int? AvgCost { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string ZipCode { get; set; }

        public string Gender { get; set; }
        public int LikeCount { get; set; }
        //public bool? Active { get; set; }

        //public string FaithFilter { get; set; }
        //public string ForFilter { get; set; }
        //public string IssuesFilter { get; set; }
        //public string InsuranceZipCodeFilter { get; set; }
        //public string GenderPreferenceFilter { get; set; }
        //public string LanguageFilter { get; set; }
        //public string LocationFilter { get; set; }
        public string TherapistTypesOfTherapyFilter { get; set; }
        public decimal TherapistRating { get; set; }
        public int TherapistRatingCount { get; set; }

        public int TotalRecords { get; set; }
    }
}