﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace theratrove.Models
{
    public class TherapistSearchResult
    {
        public int Id { get; set; }

        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string LastName { get; set; }

        [StringLength(100)]
        public string Title { get; set; } 

        [StringLength(20)]
        public string YearsInService { get; set; }

        [StringLength(100)]
        public string Education { get; set; }  

        public bool AcceptInsurance { get; set; }

        [StringLength(50)]
        public string License { get; set; }

        public string AboutMe { get; set; }

        [StringLength(200)]
        public string VideoUrl { get; set; }

        public bool? Active { get; set; }

        public DateTime? CreateDate { get; set; }

        [StringLength(50)]
        public string PhoneNumber { get; set; }

        [StringLength(50)]
        public string Location { get; set; }

        public int? AvgCost { get; set; }

        [StringLength(200)]
        public string Address1 { get; set; }

        [StringLength(200)]
        public string Address2 { get; set; }

        [StringLength(100)]
        public string City { get; set; }

        [StringLength(50)]
        public string State { get; set; }

        [StringLength(8)]
        public string ZipCode { get; set; }

        public byte? Gender { get; set; }
        public int LikeCount { get; set; }

        public int UserId { get; set; }
        public decimal? TherapistRatingAvg { get; set; }
    }
}