﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace theratrove.Models
{
    public enum FilterTypeEnum
    {
        Faith = 1,
        For = 2,
        Issues = 3,
        Insurance = 4,
        GenderPreference = 5,
        Language = 6,
        Location = 7
    }
}