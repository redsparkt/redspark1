﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace theratrove.Models
{
    public class TherapistSearchVM
    {
        public List<int> SelectedFaith { get; set; }
        public List<int> SelectedGender { get; set; }
        public List<int> SelectedFor { get; set; }
        public List<int> SelectedIssues { get; set; }
        public List<int> SelectedInsurance { get; set; }

        public int MaxDistance { get; set; }

        public List<int> SelectedLanguage { get; set; }

        //public string Location { get; set; }

        public string Keyword { get; set; }

        public int PageNo { get; set; }

        public int PageSize { get; set; }

        public string ZipCode { get; set; }
    }


}