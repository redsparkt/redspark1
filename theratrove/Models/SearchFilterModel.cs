﻿using System.Collections.Generic;
using System.Web.Mvc;
using theratrove.data;

namespace theratrove.Models
{
    public class SearchFilterModel
    {
        public SearchFilterModel()
        {
            SearchFilterList = new List<SearchFilter>();
            SearchFilterValuesList = new List<SearchFilterValue>();
            ControlTypeList = new List<SelectListItem>();
        }
        
        public int CurrentPage { get; set; }
        public List<SelectListItem> ControlTypeList { get; set; }

        public List<SearchFilter> SearchFilterList { get; set; }
        public List<SearchFilterValue> SearchFilterValuesList { get; set; }

        public SearchFilter SearchFilter { get; set; }
        public SearchFilterValue SearchFilterValue { get; set; }

        public int NoOfRecords { get; set; }

        public string RecordsRange { get; set; }
        public List<string> FilterOptionOrder;
    }

    public class SearchLogReportModel
    {

        public SearchLogReportModel()
        {
            this.SearchFilterLogList = new List<SearchFilterLog>(); 
        }
        public List<SearchFilterLog> SearchFilterLogList { get; set; }
    }
    public class TestimonialModel
    {

        public TestimonialModel()
        {
            this.TestimonialList = new List<Testimonial>();
            Testimonial = new Testimonial(); 
        }
        public List<Testimonial> TestimonialList { get; set; }
        public Testimonial Testimonial { get; set; }
    }
    public class WidgetsMode
    {
        public WidgetsMode()
        {
            this.WidgetCity = new List<Widget1>();
        }
        public List<Widget1> WidgetCity { get; set; }
        public int TotalSearches { get; set; }
    }
    public class Widget1
    {
        public int CityCount { get; set; }
        public string City { get; set; }
    }

    public class Widget2
    {
        public int Count { get; set; }
        public string Name { get; set; }
    }
}