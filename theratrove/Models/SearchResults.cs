﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace theratrove.Models
{
    public class SearchResults
    {
        public SearchResults()
        {
            TherapistList = new List<data.Therapist>();
            FilterOptions = new List<FiltersDetail>();

            this.SelectedFaith = new List<string>();
            this.SelectedFor = new List<string>();
            this.SelectedGender = new List<string>();
            this.SelectedInsurance = new List<string>();
            this.SelectedIssues = new List<string>();
            this.SelectedLanguage = new List<string>();
            this.SelectedLocation = new List<string>();
            this.RattingListData = new List<RattingList>(); 
        }
        public int CurrentPage { get; set; }

        public List<theratrove.data.Therapist> TherapistList { get; set; }
        public List<RattingList> RattingListData { get; set; }
       // public List<TherapistSearchResult> TherapistSearchResult { get; set; }

        public int NoOfRecords { get; set; }
        public int ActivePage { get; set; }

        public string RecordsRange { get; set; }
        public List<string> FilterOptionOrder;

        public List<FiltersDetail> FilterOptions { get; set; }

        public List<string> SelectedFaith { get; set; }
        public List<string> SelectedGender { get; set; }
        public List<string> SelectedFor { get; set; }
        public List<string> SelectedIssues { get; set; }
        public List<string> SelectedInsurance { get; set; }

        public List<string> SelectedLocation { get; set; }

        public List<string> SelectedLanguage { get; set; }

        public string location { get; set; }

        public string keyword { get; set; }
    }

    public class RattingList
    {
        public int Id { get; set; }
        public decimal Ratting { get; set; }
    }
}