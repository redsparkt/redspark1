﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace theratrove.Models
{
    public class FiltersDetail
    {
        public FiltersDetail()
        {
            //this.FilterValues = new List<KeyValuePair<string, string>>();
            FilterValues = new List<SelectListItem>();
        }
        public string FilterName { get; set; }

        public int FilteType { get; set; }
        //public List<KeyValuePair<string, string>> FilterValues { get; set; }

        public List<SelectListItem> FilterValues { get; set; }

        public int FilterControlType { get; set; }
    }
}