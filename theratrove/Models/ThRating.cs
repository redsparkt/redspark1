﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace theratrove.Models
{
    //public class ThRating
    //{
    //}
    public class ThRating
    {
        public decimal rating { get; set; }
        public int thid { get; set; }
        public int TherapisttId { get; set; }
    }

    public class TherapistLikeVM
    {
        
        public int UserId { get; set; }
        public int TherapisttId { get; set; }
    }
}