﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace theratrove.Helper
{
    public static class Constants
    {
        public const string AdminLogingURL = "~/Admin/Login";
        public const string UserLogingURL = "~/Home/Login";
    }
}