﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using theratrove.data;
using theratrove.Models;


namespace theratrove.Helper
{
    public class ApplicationCashManager
    {

        public static int maxDistanceValue;

        public static List<FiltersDetail> filterOptions
        {
            get
            {
                if (HttpRuntime.Cache["FilterOptions"] == null)
                {
                    //filterslist = 
                    HttpRuntime.Cache["FilterOptions"] = FillSearchFilters();
                }
                return (List<FiltersDetail>)HttpRuntime.Cache["FilterOptions"];
            }
            set
            {
                if (value != null)
                {
                    HttpRuntime.Cache["FilterOptions"] = value;
                }
            }
        }

        public static List<string> therapistsCondition
        {
            get
            {
                if (HttpRuntime.Cache["therapistsCondition"] == null)
                {
                    //filterslist = 
                    HttpRuntime.Cache["therapistsCondition"] = GetTherapistConditions();
                }
                return (List<string>)HttpRuntime.Cache["therapistsCondition"];
            }
            set
            {
                HttpRuntime.Cache["therapistsCondition"] = value;
            }
        }


        private static List<FiltersDetail> FillSearchFilters()
        {
            List<FiltersDetail> filterslist = new List<FiltersDetail>();
            using (TheratroveDB db = new TheratroveDB())
            {
                var fiters = db.SearchFilters.ToList();
                foreach (var item in fiters.OrderBy(d => d.DisplayOrderNo).ToList())
                {
                    var filterdetail = new FiltersDetail
                    {
                        FilterName = item.Name,
                        FilteType = item.FilterType,
                        FilterControlType = item.FilterControlType.Value,
                    };

                    foreach (var filtervalues in item.SearchFilterValues.Where(f => f.Active == true).OrderBy(d => d.DisplayOrder).ToList())
                    {
                        //   filterdetail.FilterValues.Add(new System.Collections.Generic.KeyValuePair<string, string>(filtervalues.Name, filtervalues.Value));
                        if (filtervalues.SearchFilter.FilterType == (int)FilterTypeEnum.Location)
                        {
                            if (maxDistanceValue < Convert.ToInt32(filtervalues.Value.ToString()))
                            {
                                maxDistanceValue = Convert.ToInt32(filtervalues.Value.ToString());
                            }
                            filterdetail.FilterValues.Add(new SelectListItem() { Text = filtervalues.Name, Value = filtervalues.Value.ToString() });
                        }
                        else
                        {
                            filterdetail.FilterValues.Add(new SelectListItem() { Text = filtervalues.Name, Value = filtervalues.Id.ToString() });
                        }
                    }

                    filterslist.Add(filterdetail);
                }
            }
            return filterslist;
        }

        private static List<string> GetTherapistConditions()
        {
            List<string> conditions = new List<string>();
            using (TheratroveDB db = new TheratroveDB())
            {
                conditions = db.TherapyTypes.Select(f => f.Name).ToList();
            }
            return conditions;
        }
    }
}