﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using static System.Net.Mime.MediaTypeNames;

namespace theratrove.Infrastructure
{
    public static class Helper
    {

        public static byte[] ImageToByte(System.Drawing.Image image, int width, int height)
        {
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(ResizeImage(image, width, height), typeof(byte[]));
        }
        public static Bitmap ResizeImage(System.Drawing.Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }
        //public static void SendEmail(MailAddress mailAddressTo,string mailBodyhtml)
        //{
        //    MailAddress mailAddress = new MailAddress("postmaster@mg.theratrove.com", "Theratrove");
        //    SmtpClient client = new SmtpClient();
        //    client.Port = 587;
        //    client.DeliveryMethod = SmtpDeliveryMethod.Network;
        //    client.UseDefaultCredentials = false;
        //    client.Credentials = new System.Net.NetworkCredential("postmaster@mg.theratrove.com", "74a367bbbcac535854dabb2a6be57270-c9270c97-4c33be12");
        //    MailMessage mail = new MailMessage(mailAddress, mailAddressTo);

        //    client.Host = "smtp.mailgun.org";
        //    mail.Subject = "Theratrove - Registration";
        //    mail.Body = mailBodyhtml;
        //    mail.IsBodyHtml = true;
        //    try
        //    {
        //        client.Send(mail);
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}
        public static void SendEmail(MailAddress mailAddressTo, string mailBodyhtml, string subject)
        {
            var fromAddress = new MailAddress("support@theratrove.com", "Theratrove");
            const string fromPassword = "74a367bbbcac535854dabb2a6be57270-c9270c97-4c33be12";


            var smtp = new SmtpClient
            {
                Host = "smtp.mailgun.org",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential("postmaster@mg.theratrove.com", fromPassword)
            };
            using (var message = new MailMessage(fromAddress, mailAddressTo)
            {
                Subject = subject,
                Body = mailBodyhtml,
                IsBodyHtml = true
            })
            {
                smtp.Send(message);
            }
        }

        public static string GetIPAddressDetails()
        {

            string IPAddress = System.Web.HttpContext.Current.Request.UserHostAddress;
            try
            {
                string rt;
                WebRequest request = WebRequest.Create("http://api.ipinfodb.com/v3/ip-city/?key=20b96dca8b9a5d37b0355e9461c66e76eed30a2274422fa6213d9de6ffb2b34e&ip=" + IPAddress);
                WebResponse response = request.GetResponse();
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                rt = reader.ReadToEnd();
                reader.Close();
                response.Close();
                return rt;
            }
            catch (Exception ex)
            {
                
            }
            return IPAddress;
        }
        public static string ToCSV<T>(this IEnumerable<T> list)
        {
            var type = typeof(T);
            var props = type.GetProperties();

            //Setup expression constants
            var param = Expression.Parameter(type, "x");
            var doublequote = Expression.Constant("\"");
            var doublequoteescape = Expression.Constant("\"\"");
            var comma = Expression.Constant(",");

            //Convert all properties to strings, escape and enclose in double quotes
            var propq = (from prop in props
                         let tostringcall = Expression.Call(Expression.Property(param, prop), prop.ReflectedType.GetMethod("ToString", new Type[0]))
                         let replacecall = Expression.Call(tostringcall, typeof(string).GetMethod("Replace", new Type[] { typeof(String), typeof(String) }), doublequote, doublequoteescape)
                         select Expression.Call(typeof(string).GetMethod("Concat", new Type[] { typeof(String), typeof(String), typeof(String) }), doublequote, replacecall, doublequote)
                         ).ToArray();

            var concatLine = propq[0];
            for (int i = 1; i < propq.Length; i++)
                concatLine = Expression.Call(typeof(string).GetMethod("Concat", new Type[] { typeof(String), typeof(String), typeof(String) }), concatLine, comma, propq[i]);

            var method = Expression.Lambda<Func<T, String>>(concatLine, param).Compile();

            var header = String.Join(",", props.Select(p => p.Name).ToArray());

            return header + Environment.NewLine + String.Join(Environment.NewLine, list.Select(method).ToArray());
        }

        public static ZipCodeList GetDistance(string zipCode, int dist)
        {
            var API = string.Format("rest/OlHbZ5XvjAfR24qLWeCDBKnsJ6V6Oonl4vswcN6iTvlE8cXRVqGd5bFj5zBjj6kE/radius.json/{0}/{1}/mile", zipCode, dist);
            //            var API = "https://www.zipcodeapi.com/rest/OlHbZ5XvjAfR24qLWeCDBKnsJ6V6Oonl4vswcN6iTvlE8cXRVqGd5bFj5zBjj6kE/radius.json/92505/10/mile";

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://www.zipcodeapi.com/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls |
                                       SecurityProtocolType.Tls11 |
                                       SecurityProtocolType.Tls12;
                //GET Method  
                HttpResponseMessage response = client.GetAsync(API).Result;
                if (response.IsSuccessStatusCode)
                {
                    var data = response.Content.ReadAsStringAsync().Result;
                    if(string.IsNullOrEmpty(data)==false)
                    return Newtonsoft.Json.JsonConvert.DeserializeObject<ZipCodeList>(data);
                }
                return null;
            }
        }

        public static string SaveImage(HttpPostedFileBase image,string folderName)
        {
            var storageAccount = CloudStorageAccount.Parse(ConfigurationManager.ConnectionStrings["BlobStorageConnectionString"].ConnectionString);
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            
            CloudBlobContainer blobContainer = blobClient.GetContainerReference(folderName);
            if (blobContainer.CreateIfNotExists())
            {
                // configure container for public access
                var permissions = blobContainer.GetPermissions();
                permissions.PublicAccess = BlobContainerPublicAccessType.Container;
                blobContainer.SetPermissions(permissions);
            }
            string uniqueBlobName = string.Format("image_{0}{1}", Guid.NewGuid().ToString(), Path.GetExtension(image.FileName));
            CloudBlockBlob blob = blobContainer.GetBlockBlobReference(uniqueBlobName);
            blob.Properties.ContentType = image.ContentType;
            blob.UploadFromStream(image.InputStream);
            return blob.Uri.ToString();
        }

        public static string SaveImageByteArray(byte[] image, string folderName,string fileName)
        {
            var storageAccount = CloudStorageAccount.Parse(ConfigurationManager.ConnectionStrings["BlobStorageConnectionString"].ConnectionString);
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            CloudBlobContainer blobContainer = blobClient.GetContainerReference(folderName);
            if (blobContainer.CreateIfNotExists())
            {
                // configure container for public access
                var permissions = blobContainer.GetPermissions();
                permissions.PublicAccess = BlobContainerPublicAccessType.Container;
                blobContainer.SetPermissions(permissions);
            }
            string uniqueBlobName = string.Format("image_{0}{1}", Guid.NewGuid().ToString(), Path.GetExtension(fileName));
            CloudBlockBlob blob = blobContainer.GetBlockBlobReference(uniqueBlobName);
            //blob.Properties.ContentType = image.ContentType;
            blob.UploadFromByteArray(image,0, image.Length);
            return blob.Uri.ToString();
        }

        public static bool DeleteImage(string urlImage,string folderName)
        {
            var storageAccount = CloudStorageAccount.Parse(ConfigurationManager.ConnectionStrings["BlobStorageConnectionString"].ConnectionString);
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer blobContainer = blobClient.GetContainerReference(folderName);
            Uri uriObj = new Uri(urlImage);
            string BlobName = Path.GetFileName(uriObj.LocalPath);
            CloudBlockBlob blockBlob = blobContainer.GetBlockBlobReference(BlobName);
            blockBlob.Delete();
            return true;
        }
    }
}

public class ZipCode
{
    public string zip_code { get; set; }
    public double distance { get; set; }
    public string city { get; set; }
    public string state { get; set; }
}

public class ZipCodeList
{
    public List<ZipCode> zip_codes { get; set; }
}